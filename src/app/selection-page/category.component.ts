import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WOW } from 'wowjs/dist/wow.min';
import * as $ from 'jquery';
import { routerTransition } from '../router.animations';

@Component({
    selector: 'app-category',
    templateUrl: './category.component.html',
    styleUrls: ['./category.component.scss'],
    animations: [routerTransition()]
})
export class CategoryComponent implements OnInit {
    constructor(public router: Router) { }
    sliders = []
    ngOnInit() {
        if ($('.navbar').length > 0) {
            $(window).on("scroll load resize", function () {
                const startY = $('.navbar').height() * 4;

                if ($(window).scrollTop() > startY) {
                    $('.navbar').addClass("scrolled");
                    $('.navbar-toggler').addClass("scrolled");
                } else {
                    $('.navbar').removeClass("scrolled");
                    $('.navbar-toggler').removeClass("scrolled");
                }
            });
        }
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();
        
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });
        this.sliders.push(
            {
                imagePath: 'http://asai-dentaloffice.com/cosmetic-dentistry-aesthetic-dentist.jpg',
                label: 'First slide label',
                text:
                    'Nulla vitae elit libero, a pharetra augue mollis interdum.'
            },
            {
                imagePath: 'http://southlondondental.com/wp-content/uploads/2017/11/gallery4.jpg',
                label: 'Second slide label',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                imagePath: 'https://wallpapersin4k.net/wp-content/uploads/2016/12/Dentist-Smile-Wallpapers-8.jpg',
                label: 'Third slide label',
                text:
                    'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
            }
        );
    }
    ngAfterViewInit() {
        new WOW().init();
    }
}
