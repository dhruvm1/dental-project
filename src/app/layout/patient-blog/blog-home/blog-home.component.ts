import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
    selector: 'app-query',
    templateUrl: './blog-home.component.html',
    styleUrls: ['./blog-home.component.scss']
})
export class BlogHomeComponent implements OnInit {
    show= false;
    constructor() { }
    ngOnInit() {
       /* $(".more-text").toggle(function(){
            $(".complete").show();    
        }, function(){
            $(".complete").hide();    
        });*/
     }
     showText(){
         if(this.show==false){
            $('.more-text').text("less..");
            $('.complete').css('display','block');
            $('.complete').addClass('animated slideInDown');
         }
         else{
            $('.more-text').text("more..");
            $('.complete').css('display','none');
         }
         this.show=!this.show;
     }
}
