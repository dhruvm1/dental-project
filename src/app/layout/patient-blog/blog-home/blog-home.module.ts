import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogHomeRoutingModule } from './blog-home.module.routing';
import { BlogHomeComponent } from './blog-home.component';

@NgModule({
    imports: [
        CommonModule,
        BlogHomeRoutingModule,
    ],
    declarations: [
        BlogHomeComponent,
    ]
})
export class BlogHomeModule {}
