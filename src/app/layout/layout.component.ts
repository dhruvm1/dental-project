import { Component, OnInit ,Input } from '@angular/core';
import * as decode from 'jwt-decode';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    constructor() {}
    name: string;
    email: string;
    phone: string;
    ngOnInit() {
        localStorage.removeItem('jwtToken');
        const token = localStorage.getItem('infoToken'); 
        const tokenPayload = decode(token);
        this.name= tokenPayload.firstname;
        this.email= tokenPayload.email;
        this.phone= tokenPayload.mobile_number;
    }
}
