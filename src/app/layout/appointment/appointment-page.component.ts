import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';
import * as decode from 'jwt-decode';

@Component({
    selector: 'app-blank-page',
    templateUrl: './appointment-page.component.html',
    styleUrls: ['./appointment-page.component.scss']
})
export class AppointmentPageComponent implements OnInit {

    isChecked = false;
    appointmentForm: FormGroup;

    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
    time = ['Morning', 'Afternoon'];

    constructor(private fb: FormBuilder) { }
    name: string;
    email: string;
    phone: string;
    ngOnInit() {
        this.appointmentForm = this.fb.group({
            radval: new FormControl(''),
            day: this.fb.array([]),
            time: this.fb.array([])
        });

        const token = localStorage.getItem('infoToken');
        const tokenPayload = decode(token);
        this.name = tokenPayload.firstname + ' ' + tokenPayload.lastname;
        this.email = tokenPayload.email;
        this.phone = tokenPayload.mobile_number;
    }

    onChange(day: string, isChecked: boolean) {
        const days = <FormArray>this.appointmentForm.controls.day;
        if (isChecked) {
            days.push(new FormControl(day));
        } else {
            let index = days.controls.findIndex(x => x.value == day)
            days.removeAt(index);
        }
    }


    onTime(time: string, isChecked: boolean) {
        const times = <FormArray>this.appointmentForm.controls.time;
        if (isChecked) {
            times.push(new FormControl(time));
        } else {
            let index = times.controls.findIndex(x => x.value == time)
            times.removeAt(index);
        }
    }


    onRequest() {

    }
}
