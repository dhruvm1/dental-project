import { AppointmentPageModule } from './appointment-page.module';

describe('BlankPageModule', () => {
    let blankPageModule: AppointmentPageModule;

    beforeEach(() => {
        blankPageModule = new AppointmentPageModule();
    });

    it('should create an instance', () => {
        expect(blankPageModule).toBeTruthy();
    });
});
