import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';
import { AppointmentPageRoutingModule } from './appointment-page-routing.module';
import { AppointmentPageComponent } from './appointment-page.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {MatFormFieldModule} from '@angular/material';

@NgModule({
    imports: [CommonModule, AppointmentPageRoutingModule,MatCheckboxModule,FormsModule,ReactiveFormsModule,MatRadioModule,MatFormFieldModule],
    declarations: [AppointmentPageComponent]
})
export class AppointmentPageModule {}
