import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
    NbMediaBreakpoint,
    NbMediaBreakpointsService,
    NbMenuItem,
    NbMenuService,
    NbSidebarService,
    NbThemeService,
} from '@nebular/theme';

import * as $ from 'jquery';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        if (event.target.innerWidth >= 990) {
            $('.bars').addClass('change');
            $('.sidebar').css('display', 'block');
            $('.bars').css('margin-left', '210px');
        }
        else {
            $('.bars').removeClass('change');
            $('.sidebar').css('display', 'none');
            $('.bars').css('margin-left', '0px');
        }
    }

    isActive: boolean = false;
    showMenu = false;
    showMenu2 = false;
    showMenu3 = false;
    pushRightClass: string = 'push-right';
    @Input() name: string;
    user_name: string;

    ngOnInit() {

    }

    constructor(private translate: TranslateService, public router: Router) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }
    togglebar() {
        if (this.showMenu3 == false) {
            $('.bars').addClass('change');
            $('.bars').css('margin-left', '210px');
            $('.sidebar').css('display', 'block');
        }
        else {
            $('.bars').removeClass('change');
            $('.bars').css('margin-left', '0px');
            $('.sidebar').css('display', 'none');
        }
        this.showMenu3 = !this.showMenu3;
    }
    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass() {
        this.showMenu = !this.showMenu;
    }
    MenuExpandClass() {
        this.showMenu2 = !this.showMenu2;
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('infoToken');
        this.router.navigate(['/home']);
    }
}
