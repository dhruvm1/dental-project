import { Injectable,OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import * as decode from 'jwt-decode';

@Injectable()
export class HeaderService implements OnInit{
    loading: boolean;
    loading_subject = new Subject<boolean>();
    constructor(private http: HttpClient, private router: Router) { }
    ngOnInit(){
        const token = localStorage.getItem('infoToken'); 
        const tokenPayload = decode(token);
        console.log(tokenPayload);
    }
}