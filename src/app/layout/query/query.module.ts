import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { QueryRoutingModule } from './query.module.routing';
import { QueryComponent } from './query.component';
import { StatModule } from '../../shared';
import { ChatService } from './query.service';

@NgModule({
    imports: [
        CommonModule,
        QueryRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule
    ],
    declarations: [
        QueryComponent,
    ],
    providers: [ChatService,{provide: LocationStrategy, useClass: HashLocationStrategy}]
})
export class QueryModule {}
