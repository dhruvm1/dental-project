import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientHomeRoutingModule } from './patient-home-routing.module';
import { PatientHomeComponent } from './patient-home.component';

@NgModule({
    imports: [
        CommonModule,
        PatientHomeRoutingModule,
    ],
    declarations: [
        PatientHomeComponent,
    ]
})
export class PatientHomeModule {}
