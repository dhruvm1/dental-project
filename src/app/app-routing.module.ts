import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import 'hammerjs';
import { AuthGuard } from './shared';
import { DashAuthGuard } from './shared/guard/loginsignauth.guard';

const routes: Routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule',canActivate:[DashAuthGuard]},
    { path:'home', loadChildren: './selection-page/category.module#CategoryModule' },
    { path: 'login/patient', loadChildren: './patient-login/login.module#LoginModule'},
    { path: 'patient/information', loadChildren: './patient-info/patient-info.module#PatientInfoModule',canActivate:[AuthGuard]},
    { path: 'login/dentist', loadChildren: './dentist-login/dentist-login.module#DentistLoginModule'},
    { path: 'signup/patient', loadChildren: './signup/signup.module#SignupModule'},
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: 'login', redirectTo: 'category-select'},
    { path: 'signup', redirectTo: 'signup/patient'},
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes,{useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {}
