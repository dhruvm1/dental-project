import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import {MatInputModule,MatSelectModule,MatFormFieldModule, ErrorStateMatcher,MatIconModule,MatSnackBarModule} from '@angular/material';
import { LoadingModule ,ANIMATION_TYPES } from 'ngx-loading';

import { SignupRoutingModule } from './signup-routing.module';
import { SignupService } from './signup-service.service';
import { SignupComponent } from './signup.component';

@NgModule({
  imports: [
    CommonModule,
    SignupRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatIconModule,
    MatSnackBarModule,
    NgbCarouselModule.forRoot(),
    NgbAlertModule.forRoot(),
    LoadingModule.forRoot({
      animationType: ANIMATION_TYPES.rectangleBounce,
      backdropBackgroundColour: 'rgba(0,0,0,0.5)', 
      backdropBorderRadius: '4px',
      primaryColour: '#ffffff', 
      secondaryColour: '#ffffff', 
      tertiaryColour: '#ffffff'
  })
  ],
  declarations: [SignupComponent],
  providers: [SignupService]
})
export class SignupModule { }
