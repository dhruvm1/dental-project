import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MatInputModule, MatSelectModule, MatFormFieldModule, ErrorStateMatcher, MatSnackBarConfig } from '@angular/material';
import { SignupService } from './signup-service.service';
import { WOW } from 'wowjs/dist/wow.min';
import { routerTransition } from '../router.animations';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    constructor(private fb: FormBuilder, private signupService: SignupService) {
    }
    hide = true;
    show = false;
    loading: boolean;
    registerForm: FormGroup;
    repassword: string;
    password: string;
    onKey(event: KeyboardEvent) {
        this.repassword = (<HTMLInputElement>event.target).value;
        if (this.repassword == this.password) {
            this.show = false;
        }
        else {
            this.show = true;
        }
    }
    ngOnInit() {
        this.registerForm = this.fb.group({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [Validators.required]),
            repassword: new FormControl('', [Validators.required])
        });
        this.signupService.loading_subject.subscribe((load) => {
            this.loading = load;
        });
    }
    ngAfterViewInit() {
        new WOW().init();
    }

    onRegister() {
        if (this.repassword == this.password) {
            var credentials = {
                email: this.registerForm.value.email,
                password: this.registerForm.value.password
            }
            this.signupService.RegisterWithEmailAndPassword(credentials);
            this.registerForm.reset();
        }
    }
}
