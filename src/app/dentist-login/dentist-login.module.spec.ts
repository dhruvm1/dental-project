import { DentistLoginModule } from './dentist-login.module';

describe('LoginModule', () => {
    let loginModule: DentistLoginModule;

    beforeEach(() => {
        loginModule = new DentistLoginModule();
    });

    it('should create an instance', () => {
        expect(loginModule).toBeTruthy();
    });
});
