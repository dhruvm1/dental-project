import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DentistLoginComponent } from './dentist-login.component';

describe('LoginComponent', () => {
  let component: DentistLoginComponent;
  let fixture: ComponentFixture<DentistLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DentistLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DentistLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
