import { Component, OnInit, ViewChild} from '@angular/core';
import { PlatformLocation } from '@angular/common'
//import { HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
import {FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import {MatInputModule,MatSelectModule,MatFormFieldModule, ErrorStateMatcher} from '@angular/material';
import { WOW } from 'wowjs/dist/wow.min';
import { routerTransition } from '../router.animations';

@Component({
    selector: 'app-login',
    templateUrl: './dentist-login.component.html',
    styleUrls: ['./dentist-login.component.scss'],
    animations: [routerTransition()]
})
export class DentistLoginComponent implements OnInit {
   /* @HostListener('window:popstate', ['$event'])
    
    onPopState(event) {
        localStorage.clear();
    }*/

    loginForm : FormGroup;
    
    constructor(private fb: FormBuilder) {}

    category:string;
    loading=false;

    ngOnInit() {
        this.loginForm= this.fb.group({
            uniqueId : new FormControl('', [Validators.required]),
            username : new FormControl('', [Validators.required]),
            password : new FormControl('', [Validators.required]) 
        });
    }
    ngAfterViewInit(){
        new WOW().init();
    }
    
    onLoggedin() {
        console.log(this.loginForm.value);
        localStorage.setItem('isLoggedin', 'true');
        console.log("hello"+ localStorage.getItem('isLoggedin'));
        this.loading=true;
        this.loginForm.reset();
    }
}
