import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule,MatSelectModule,MatFormFieldModule,MatIconModule} from '@angular/material';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';
import { LoadingModule ,ANIMATION_TYPES } from 'ngx-loading';

import { DentistLoginRoutingModule } from './dentist-login-routing.module';
import { DentistLoginComponent } from './dentist-login.component';

@NgModule({
    imports: [CommonModule, MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        MatIconModule,
        ReactiveFormsModule,
        DentistLoginRoutingModule,FormsModule,LoadingModule.forRoot({
        animationType: ANIMATION_TYPES.rectangleBounce,
        backdropBackgroundColour: 'rgba(0,0,0,0.5)', 
        backdropBorderRadius: '4px',
        primaryColour: '#ffffff', 
        secondaryColour: '#ffffff', 
        tertiaryColour: '#ffffff'
    })],
    declarations: [DentistLoginComponent]
})
export class DentistLoginModule {}
