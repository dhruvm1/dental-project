import { Component, OnInit, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common'
//import { HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { MatInputModule, MatSelectModule, MatFormFieldModule, ErrorStateMatcher } from '@angular/material';
import { WOW } from 'wowjs/dist/wow.min';
import { routerTransition } from '../router.animations';
import { PatientLoginService } from './login.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    /* @HostListener('window:popstate', ['$event'])
     
     onPopState(event) {
         localStorage.clear();
     }*/

    loginForm: FormGroup;

    constructor(private fb: FormBuilder, private loginService: PatientLoginService) { }

    loading: boolean;
    hide = true;

    ngOnInit() {
        this.loginForm = this.fb.group({
            email: new FormControl('', [Validators.required, Validators.email]),
            password: new FormControl('', [Validators.required])
        });
        this.loginService.loading_subject.subscribe((load) => {
            this.loading = load;
        })
    }
    ngAfterViewInit() {
        new WOW().init();
    }

    onLoggedin() {
        this.loginService.LoginWithEmailAndPassword(this.loginForm.value);
        this.loginForm.reset();
    }
}
