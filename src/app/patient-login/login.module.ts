import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule,MatSelectModule,MatFormFieldModule,MatIconModule,MatSnackBarModule} from '@angular/material';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';
import { LoadingModule ,ANIMATION_TYPES } from 'ngx-loading';

import { LoginRoutingModule } from './login-routing.module';
import { PatientLoginService } from './login.service';
import { LoginComponent } from './login.component';

@NgModule({
    imports: [CommonModule, MatInputModule,
        MatSelectModule,
        MatFormFieldModule,
        MatIconModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        LoginRoutingModule,FormsModule,LoadingModule.forRoot({
        animationType: ANIMATION_TYPES.rectangleBounce,
        backdropBackgroundColour: 'rgba(0,0,0,0.5)', 
        backdropBorderRadius: '4px',
        primaryColour: '#ffffff', 
        secondaryColour: '#ffffff', 
        tertiaryColour: '#ffffff'
    })],
    declarations: [LoginComponent],
    providers: [PatientLoginService]
})
export class LoginModule {}
