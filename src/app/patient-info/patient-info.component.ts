import { Component, OnInit, ViewChild } from '@angular/core';
import { PlatformLocation } from '@angular/common'
//import { HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router, Data } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { MatInputModule, MatSelectModule, MatFormFieldModule, ErrorStateMatcher } from '@angular/material';
import { routerTransition } from '../router.animations';
import { PatientInfoService } from './patient-info.service';

@Component({
    selector: 'app-login',
    templateUrl: './patient-info.component.html',
    styleUrls: ['./patient-info.component.scss'],
    animations: [routerTransition()]
})
export class PatientInfoComponent implements OnInit {
    /* @HostListener('window:popstate', ['$event'])
     
     onPopState(event) {
         localStorage.clear();
     }*/
    newItem = {
        EndTime: null,
        StartTime: null
    };

    loginForm: FormGroup;

    constructor(private fb: FormBuilder, private infoService: PatientInfoService,private router: Router) { }

    loading: boolean;
    hide = true;

    ngOnInit() {
        this.loginForm = this.fb.group({
            firstname: new FormControl('', [Validators.required]),
            lastname: new FormControl('', [Validators.required]),
            mobile_number: new FormControl('', [Validators.required]),
            gender: new FormControl('', [Validators.required]),
            citizen: new FormControl('', [Validators.required]),
            dob: new FormControl('', [Validators.required]),
            street_address: new FormControl('', [Validators.required]),
            street_address2: new FormControl('', [Validators.required]),
            landmark: new FormControl('', [Validators.required]),
            city: new FormControl('', [Validators.required]),
            state: new FormControl('', [Validators.required]),
            code: new FormControl('', [Validators.required]),
        });
    }
    OnSubmit() {
        if(localStorage.getItem('jwtToken')!=null){
        this.infoService.storeInformationOfPatient(this.loginForm.value);
        }
        else{
            this.router.navigate(['/signup/patient']);
        }
    }
}
