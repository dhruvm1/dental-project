webpackJsonp(["patient-info.module"],{

/***/ "../../../../../src/app/patient-info/patient-info.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"w3ls-banner\">\n    <div class=\"heading\">\n        <h1>General Application Form</h1>\n    </div>\n    <div class=\"container\">\n        <div class=\"heading\">\n            <h2>Please Enter Your Details</h2>\n            <p>Fill the application form below and submit.</p>\n        </div>\n        <div class=\"agile-form\">\n            <form action=\"#\" [formGroup]=\"loginForm\">\n                <ul class=\"field-list\">\n                    <li class=\"name\">\n                        <label class=\"form-label\" style=\"padding-top:20px;\"> Name\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input add\">\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"firstname\" placeholder=\"First Name\" required>\n                                <mat-error *ngIf=\"loginForm.get('firstname').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            &nbsp;\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"lastname\" placeholder=\"Last Name\" required>\n                                <mat-error *ngIf=\"loginForm.get('lastname').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </li>\n                    <li>\n                        <label class=\"form-label\" style=\"padding-top:10px\"> Phone Number\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input add\">\n                            <mat-form-field>\n                                <input matInput type=\"number\" style=\"background:white;height:30px;color:black\" formControlName=\"mobile_number\" placeholder=\"Phone Number\"\n                                    required>\n                                <mat-error *ngIf=\"loginForm.get('mobile_number').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </li>\n                    <li>\n                        <label class=\"form-label\"> Gender\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input\">\n                            <select class=\"form-dropdown\" formControlName=\"gender\" required>\n                                <option value=\"\">Gender</option>\n                                <option value=\"Male\"> Male </option>\n                                <option value=\"Female\"> Female </option>\n                                <option value=\"Transgender\"> Transgender </option>\n                            </select>\n                        </div>\n                    </li>\n                    <li>\n                        <label class=\"form-label\"> Citizen\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input\">\n                            <select class=\"form-dropdown\" formControlName=\"citizen\" required>\n                                <option value=\"\">Select Country</option>\n                                <option value=\"United states\"> United states </option>\n                                <option value=\"Afghanisthan\"> Afghanistan </option>\n                                <option value=\"China\"> China </option>\n                                <option value=\"Indonesia\"> Indonesia </option>\n                                <option value=\"England\"> England </option>\n                                <option value=\"Others\"> Others </option>\n                            </select>\n                        </div>\n                    </li>\n                    <li>\n                        <label class=\"form-label\"> Date of Birth\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input dob\">\n                            <div class=\"input-group\">\n                                <input class=\"form-control\" formControlName=\"dob\" placeholder=\"yyyy-mm-dd\" [(ngModel)]=\"model\" ngbDatepicker #d=\"ngbDatepicker\"\n                                    required>\n                                <div class=\"input-group-append\">\n                                    <button class=\"btn btn-outline-secondary\" (click)=\"d.toggle()\" type=\"button\">\n                                        <i class=\"fa fa-calendar\"></i>\n                                    </button>\n                                </div>\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <label class=\"form-label\">\n                            Address\n                            <span class=\"form-required\"> * </span>\n                        </label>\n                        <div class=\"form-input add\">\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"street_address\" placeholder=\"Street Address \" required>\n                                <mat-error *ngIf=\"loginForm.get('street_address').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"street_address2\" placeholder=\"Street Line 2 \" required>\n                                <mat-error *ngIf=\"loginForm.get('street_address').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"landmark\" placeholder=\"Landmark \" required>\n                                <mat-error *ngIf=\"loginForm.get('landmark').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"city\" placeholder=\"City\" required>\n                                <mat-error *ngIf=\"loginForm.get('city').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"state\" placeholder=\"State / Province \" required>\n                                <mat-error *ngIf=\"loginForm.get('state').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                            <mat-form-field>\n                                <input matInput type=\"text\" formControlName=\"code\" placeholder=\"Postal / Zip Code \" required>\n                                <mat-error *ngIf=\"loginForm.get('code').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </li>\n                </ul>\n                <div class=\"submit_btn\">\n                    <input type=\"submit\" value=\"Submit\" [disabled]=\"!loginForm.valid\" (click)=\"OnSubmit()\">\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/patient-info/patient-info.component.scss":
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__("../../../../css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "/*-- //Reset-Code --*/\n/* respoinsive */\n/* //respoinsive */\nbody {\n  padding: 0;\n  margin: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nbody a {\n    transition: 0.5s all;\n    -webkit-transition: 0.5s all;\n    -moz-transition: 0.5s all;\n    -o-transition: 0.5s all;\n    -ms-transition: 0.5s all;\n    text-decoration: none; }\nbody a:hover {\n      text-decoration: none; }\nbody a:focus {\n      text-decoration: none; }\na:hover {\n  text-decoration: none;\n  text-decoration: none;\n  outline: none; }\na:focus {\n  text-decoration: none;\n  outline: none; }\ninput[type=\"button\"] {\n  transition: 0.5s all;\n  -webkit-transition: 0.5s all;\n  -moz-transition: 0.5s all;\n  -o-transition: 0.5s all;\n  -ms-transition: 0.5s all; }\ninput[type=\"submit\"] {\n  transition: 0.5s all;\n  -webkit-transition: 0.5s all;\n  -moz-transition: 0.5s all;\n  -o-transition: 0.5s all;\n  -ms-transition: 0.5s all;\n  width: 72.5%;\n  padding: 10px 40px;\n  margin-top: 5px;\n  font-size: 16px;\n  background-color: #007cc0;\n  border: 1px solid #007cc0;\n  color: #fff;\n  text-transform: uppercase;\n  letter-spacing: 1px;\n  outline: 0;\n  cursor: pointer;\n  font-family: 'Alegreya Sans', sans-serif; }\ninput[type=\"submit\"]:hover {\n    background-color: #ffc168;\n    border: 1px solid #ffc168;\n    color: #000; }\nh1 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nh2 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nh3 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nh4 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nh5 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\nh6 {\n  margin: 0;\n  padding: 0;\n  font-family: 'Alegreya Sans', sans-serif; }\np {\n  margin: 0; }\nul {\n  margin: 0;\n  padding: 0; }\nli {\n  list-style-type: none; }\nlabel {\n  margin: 0; }\n.w3ls-banner {\n  background: url(" + escape(__webpack_require__("../../../../../src/assets/images/bg.jpg")) + ") no-repeat;\n  background-size: cover;\n  min-height: 100vh;\n  color: white;\n  background-position: center;\n  padding-top: 30px; }\n.heading {\n  text-align: center;\n  margin-bottom: 25px;\n  letter-spacing: 1px; }\n.heading h1 {\n    margin-bottom: 25px;\n    text-transform: uppercase;\n    font-weight: 500;\n    font-size: 50px; }\n.heading h2 {\n    font-weight: 400; }\n.heading p {\n    font-size: 16px;\n    margin-top: 20px; }\n.container {\n  width: 43%;\n  margin: auto;\n  padding: 30px 30px 20px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  background-color: rgba(0, 0, 0, 0.5); }\nlabel.form-label {\n  display: inline-block;\n  width: 27%;\n  font-weight: 400;\n  letter-spacing: 1px;\n  vertical-align: top; }\n.form-input {\n  display: inline-block;\n  width: 72%; }\n.name .form-input {\n  display: inline-block;\n  width: 72%; }\n.name span.form-sub-label input[type=\"text\"] {\n  margin-bottom: 0; }\nul.field-list li {\n  margin: 15px 0px; }\nselect {\n  width: 98.4%;\n  padding: 12px 10px;\n  color: #666;\n  border: none;\n  font-size: 15px;\n  font-family: 'Alegreya Sans', sans-serif;\n  background-color: #fff; }\nselect:focus {\n    outline: none; }\ninput[type=\"text\"] {\n  padding: 12px 10px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  background-color: #fff;\n  font-size: 15px;\n  border: none;\n  width: 98.6%;\n  color: #333;\n  font-family: 'Alegreya Sans', sans-serif; }\ninput[type=\"text\"]:focus {\n    outline: none; }\ninput[type=\"email\"] {\n  padding: 12px 10px;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  background-color: #fff;\n  font-size: 15px;\n  border: none;\n  width: 98.6%;\n  color: #333;\n  font-family: 'Alegreya Sans', sans-serif; }\ninput[type=\"email\"]:focus {\n    outline: none; }\n.dob select {\n  width: 32.4%; }\n.dob span.form-sub-label input[type=\"text\"] {\n  margin-bottom: 0; }\nspan.form-sub-label input[type=\"text\"] {\n  margin: 0px 0px 15px 0;\n  width: 49%; }\nspan.last input[type=\"text\"] {\n  margin: 0px 0px 0px 0; }\nspan.form-required {\n  color: #fff; }\nlabel.form-label1 {\n  display: inline-block;\n  margin: 15px 0px;\n  letter-spacing: 1px; }\nlabel.type-of-test {\n  display: inline-block;\n  width: 150px;\n  margin-bottom: 10px;\n  margin-left: 10px;\n  letter-spacing: 1px;\n  font-weight: 100;\n  font-size: 14px; }\ntextarea {\n  width: 98%;\n  background-color: rgba(255, 255, 255, 0.63); }\n.submit_btn {\n  text-align: right;\n  margin-right: 10px; }\ninput.year {\n  width: 31% !important; }\ninput[type=\"checkbox\"] {\n  vertical-align: middle; }\n.copyright p {\n  text-align: center;\n  padding: 15px 5px;\n  letter-spacing: 1px;\n  font-size: 16px; }\n.copyright a {\n  color: #fff; }\n.copyright a:hover {\n    color: #ffc168; }\n@media screen and (max-width: 1366px) {\n  .container {\n    width: 45%; }\n  .submit_btn {\n    margin-right: 8px; }\n  input[type=\"submit\"] {\n    width: 72%; } }\n@media screen and (max-width: 1280px) {\n  .heading h1 {\n    font-size: 45px; } }\n@media screen and (max-width: 1080px) {\n  .container {\n    width: 52%; } }\n@media screen and (max-width: 991px) {\n  .container {\n    width: 55%; }\n  .heading h1 {\n    font-size: 42px; } }\n@media screen and (max-width: 900px) {\n  .container {\n    width: 58%; }\n  .heading h1 {\n    font-size: 40px; } }\n@media screen and (max-width: 800px) {\n  .container {\n    width: 65%; }\n  .heading h1 {\n    font-size: 38px; } }\n@media screen and (max-width: 768px) {\n  .container {\n    width: 70%; }\n  .heading h1 {\n    font-size: 36px; } }\n@media screen and (max-width: 736px) {\n  .container {\n    width: 80%; } }\n@media screen and (max-width: 640px) {\n  .container {\n    width: 85%; } }\n@media screen and (max-width: 568px) {\n  .container {\n    width: 90%; }\n  label.type-of-test {\n    width: 40%; }\n  label.form-sub-label1 {\n    position: absolute;\n    bottom: -28px;\n    left: 5px;\n    font-size: 13px;\n    display: block; } }\n@media screen and (max-width: 480px) {\n  .heading h1 {\n    font-size: 33px; }\n  .heading h2 {\n    font-size: 1.3em; }\n  .container {\n    width: 95%;\n    padding: 17px 10px 20px; }\n  label.form-label {\n    font-size: 15px; }\n  .copyright p {\n    line-height: 26px; } }\n@media screen and (max-width: 414px) {\n  .form-input {\n    width: 100%; }\n  label.form-label {\n    width: 100%;\n    margin-bottom: 7px; }\n  input.year {\n    width: 41% !important; }\n  label.form-sub-label1 {\n    bottom: -22px;\n    left: 4px;\n    font-size: 13px;\n    letter-spacing: 1px; }\n  .heading h2 {\n    font-size: 22px;\n    font-size: 20px; }\n  .heading h1 {\n    font-size: 29px; }\n  .heading p {\n    font-size: 15px;\n    margin-top: 10px; }\n  .dob select {\n    width: 32%; }\n  input[type=\"email\"] {\n    width: 99%;\n    padding: 10px 10px; }\n  textarea {\n    width: 97%; }\n  input[type=\"text\"] {\n    padding: 10px 10px; }\n  select {\n    padding: 10px 10px;\n    padding-right: 0; }\n  .name .form-input {\n    width: 100%; }\n  input[type=\"submit\"] {\n    width: 100%; }\n  .submit_btn {\n    margin-right: 4px; } }\n@media screen and (max-width: 384px) {\n  .heading h1 {\n    margin-bottom: 20px;\n    font-size: 30px;\n    font-size: 28px; } }\n@media screen and (max-width: 375px) {\n  label.type-of-test {\n    width: 40%;\n    margin-left: 5px; }\n  label.form-sub-label1 {\n    bottom: -25px; }\n  input.year {\n    width: 39% !important; }\n  .heading h2 {\n    font-size: 20px; }\n  .heading p {\n    font-size: 15px;\n    margin-top: 15px; }\n  .heading h1 {\n    font-size: 26px; }\n  .copyright p {\n    font-size: 15px; } }\n@media screen and (max-width: 320px) {\n  .heading h1 {\n    margin-bottom: 20px;\n    font-size: 24px; }\n  .heading h2 {\n    font-size: 17px; }\n  .heading p {\n    font-size: 13px;\n    margin-top: 15px; }\n  span.form-sub-label input[type=\"text\"] {\n    width: 99%; }\n  span.last input[type=\"text\"] {\n    margin: 0px 0px 15px 0; }\n  label.type-of-test {\n    width: 36%;\n    margin-left: 7px;\n    font-size: 14px; }\n  input[type=\"text\"] {\n    width: 99%; }\n  label.form-label1 {\n    margin: 8px 0px; }\n  .copyright p {\n    font-size: 14px; }\n  .name span.form-sub-label input[type=\"text\"]:nth-child(1) {\n    margin-bottom: 10px; }\n  ul.field-list li {\n    margin: 10px 0px; }\n  input[type=\"submit\"] {\n    font-size: 15px;\n    margin-top: 0px; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/patient-info/patient-info.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var router_animations_1 = __webpack_require__("../../../../../src/app/router.animations.ts");
var patient_info_service_1 = __webpack_require__("../../../../../src/app/patient-info/patient-info.service.ts");
var PatientInfoComponent = (function () {
    function PatientInfoComponent(fb, infoService, router) {
        this.fb = fb;
        this.infoService = infoService;
        this.router = router;
        /* @HostListener('window:popstate', ['$event'])
         
         onPopState(event) {
             localStorage.clear();
         }*/
        this.newItem = {
            EndTime: null,
            StartTime: null
        };
        this.hide = true;
    }
    PatientInfoComponent.prototype.ngOnInit = function () {
        this.loginForm = this.fb.group({
            firstname: new forms_1.FormControl('', [forms_1.Validators.required]),
            lastname: new forms_1.FormControl('', [forms_1.Validators.required]),
            mobile_number: new forms_1.FormControl('', [forms_1.Validators.required]),
            gender: new forms_1.FormControl('', [forms_1.Validators.required]),
            citizen: new forms_1.FormControl('', [forms_1.Validators.required]),
            dob: new forms_1.FormControl('', [forms_1.Validators.required]),
            street_address: new forms_1.FormControl('', [forms_1.Validators.required]),
            street_address2: new forms_1.FormControl('', [forms_1.Validators.required]),
            landmark: new forms_1.FormControl('', [forms_1.Validators.required]),
            city: new forms_1.FormControl('', [forms_1.Validators.required]),
            state: new forms_1.FormControl('', [forms_1.Validators.required]),
            code: new forms_1.FormControl('', [forms_1.Validators.required]),
        });
    };
    PatientInfoComponent.prototype.OnSubmit = function () {
        if (localStorage.getItem('jwtToken') != null) {
            this.infoService.storeInformationOfPatient(this.loginForm.value);
        }
        else {
            this.router.navigate(['/signup/patient']);
        }
    };
    PatientInfoComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/patient-info/patient-info.component.html"),
            styles: [__webpack_require__("../../../../../src/app/patient-info/patient-info.component.scss")],
            animations: [router_animations_1.routerTransition()]
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder, patient_info_service_1.PatientInfoService, router_1.Router])
    ], PatientInfoComponent);
    return PatientInfoComponent;
}());
exports.PatientInfoComponent = PatientInfoComponent;


/***/ }),

/***/ "../../../../../src/app/patient-info/patient-info.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var ngx_loading_1 = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
var patient_info_routing_module_1 = __webpack_require__("../../../../../src/app/patient-info/patient-info.routing.module.ts");
var patient_info_service_1 = __webpack_require__("../../../../../src/app/patient-info/patient-info.service.ts");
var patient_info_component_1 = __webpack_require__("../../../../../src/app/patient-info/patient-info.component.ts");
exports.MY_NATIVE_FORMATS = {
    fullPickerInput: { year: 'numeric', month: 'numeric', day: 'numeric', hour: 'numeric', minute: 'numeric' },
    datePickerInput: { year: 'numeric', month: 'numeric', day: 'numeric' },
    timePickerInput: { hour: 'numeric', minute: 'numeric' },
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
};
var PatientInfoModule = (function () {
    function PatientInfoModule() {
    }
    PatientInfoModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, material_1.MatInputModule,
                material_1.MatSelectModule,
                material_1.MatFormFieldModule,
                material_1.MatIconModule,
                forms_1.ReactiveFormsModule,
                ng_bootstrap_1.NgbModule.forRoot(),
                material_1.MatSnackBarModule,
                patient_info_routing_module_1.PatientInfoRoutingModule, forms_1.FormsModule, ngx_loading_1.LoadingModule.forRoot({
                    animationType: ngx_loading_1.ANIMATION_TYPES.rectangleBounce,
                    backdropBackgroundColour: 'rgba(0,0,0,0.5)',
                    backdropBorderRadius: '4px',
                    primaryColour: '#ffffff',
                    secondaryColour: '#ffffff',
                    tertiaryColour: '#ffffff'
                })],
            declarations: [patient_info_component_1.PatientInfoComponent],
            providers: [patient_info_service_1.PatientInfoService]
        })
    ], PatientInfoModule);
    return PatientInfoModule;
}());
exports.PatientInfoModule = PatientInfoModule;


/***/ }),

/***/ "../../../../../src/app/patient-info/patient-info.routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var patient_info_component_1 = __webpack_require__("../../../../../src/app/patient-info/patient-info.component.ts");
var routes = [
    {
        path: '',
        component: patient_info_component_1.PatientInfoComponent
    }
];
var PatientInfoRoutingModule = (function () {
    function PatientInfoRoutingModule() {
    }
    PatientInfoRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PatientInfoRoutingModule);
    return PatientInfoRoutingModule;
}());
exports.PatientInfoRoutingModule = PatientInfoRoutingModule;


/***/ }),

/***/ "../../../../../src/app/patient-info/patient-info.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var PatientInfoService = (function () {
    function PatientInfoService(http, router, snackBar) {
        this.http = http;
        this.router = router;
        this.snackBar = snackBar;
        this.loading_subject = new Subject_1.Subject();
    }
    PatientInfoService.prototype.storeInformationOfPatient = function (info) {
        var _this = this;
        var httpOptions = {
            headers: new http_1.HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken') })
        };
        this.http.post('/api/storePatientInformation', info, httpOptions).subscribe(function (response) {
            if (response[0].success == true) {
                localStorage.setItem('infoToken', response[0].infoToken);
                if (localStorage.getItem('firstTime') == null) {
                    _this.router.navigate(['/login/patient']);
                    _this.loading = false;
                    _this.loading_subject.next(_this.loading);
                }
                else {
                    _this.router.navigate(['/dashboard']);
                    _this.loading = false;
                    _this.loading_subject.next(_this.loading);
                }
            }
        }, function (error) {
            console.log(error);
        });
    };
    PatientInfoService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router, material_1.MatSnackBar])
    ], PatientInfoService);
    return PatientInfoService;
}());
exports.PatientInfoService = PatientInfoService;


/***/ }),

/***/ "../../../../../src/assets/images/bg.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "bg.0cbe786f1b545ebaf897.jpg";

/***/ }),

/***/ "../../../../css-loader/lib/url/escape.js":
/***/ (function(module, exports) {

module.exports = function escape(url) {
    if (typeof url !== 'string') {
        return url
    }
    // If url is already wrapped in quotes, remove them
    if (/^['"].*['"]$/.test(url)) {
        url = url.slice(1, -1);
    }
    // Should url be wrapped?
    // See https://drafts.csswg.org/css-values-3/#urls
    if (/["'() \t\n]/.test(url)) {
        return '"' + url.replace(/"/g, '\\"').replace(/\n/g, '\\n') + '"'
    }

    return url
}


/***/ })

});
//# sourceMappingURL=patient-info.module.chunk.js.map