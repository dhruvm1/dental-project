webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./access-denied/access-denied.module": [
		"../../../../../src/app/access-denied/access-denied.module.ts",
		"access-denied.module"
	],
	"./appointment/appointment-page.module": [
		"../../../../../src/app/layout/appointment/appointment-page.module.ts",
		"appointment-page.module",
		"common"
	],
	"./blog-home/blog-home.module": [
		"../../../../../src/app/layout/patient-blog/blog-home/blog-home.module.ts",
		"blog-home.module"
	],
	"./bs-component/bs-component.module": [
		"../../../../../src/app/layout/bs-component/bs-component.module.ts",
		"bs-component.module"
	],
	"./charts/charts.module": [
		"../../../../../src/app/layout/charts/charts.module.ts",
		"charts.module",
		"common"
	],
	"./dashboard/dashboard.module": [
		"../../../../../src/app/layout/dashboard/dashboard.module.ts",
		"dashboard.module"
	],
	"./dentist-login/dentist-login.module": [
		"../../../../../src/app/dentist-login/dentist-login.module.ts"
	],
	"./form/form.module": [
		"../../../../../src/app/layout/form/form.module.ts",
		"form.module"
	],
	"./layout/layout.module": [
		"../../../../../src/app/layout/layout.module.ts",
		"layout.module",
		"common"
	],
	"./not-found/not-found.module": [
		"../../../../../src/app/not-found/not-found.module.ts",
		"not-found.module"
	],
	"./patient-blog/blog.module": [
		"../../../../../src/app/layout/patient-blog/blog.module.ts",
		"blog.module"
	],
	"./patient-info/patient-info.module": [
		"../../../../../src/app/patient-info/patient-info.module.ts",
		"patient-info.module"
	],
	"./patient-login/login.module": [
		"../../../../../src/app/patient-login/login.module.ts",
		"login.module"
	],
	"./query/query.module": [
		"../../../../../src/app/layout/query/query.module.ts",
		"query.module",
		"common"
	],
	"./selection-page/category.module": [
		"../../../../../src/app/selection-page/category.module.ts"
	],
	"./server-error/server-error.module": [
		"../../../../../src/app/server-error/server-error.module.ts",
		"server-error.module"
	],
	"./signup/signup.module": [
		"../../../../../src/app/signup/signup.module.ts",
		"signup.module"
	],
	"./tables/tables.module": [
		"../../../../../src/app/layout/tables/tables.module.ts",
		"tables.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/app-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
__webpack_require__("../../../../hammerjs/hammer.js");
var shared_1 = __webpack_require__("../../../../../src/app/shared/index.ts");
var loginsignauth_guard_1 = __webpack_require__("../../../../../src/app/shared/guard/loginsignauth.guard.ts");
var routes = [
    { path: '', loadChildren: './layout/layout.module#LayoutModule', canActivate: [loginsignauth_guard_1.DashAuthGuard] },
    { path: 'home', loadChildren: './selection-page/category.module#CategoryModule' },
    { path: 'login/patient', loadChildren: './patient-login/login.module#LoginModule' },
    { path: 'patient/information', loadChildren: './patient-info/patient-info.module#PatientInfoModule', canActivate: [shared_1.AuthGuard] },
    { path: 'login/dentist', loadChildren: './dentist-login/dentist-login.module#DentistLoginModule' },
    { path: 'signup/patient', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: 'login', redirectTo: 'category-select' },
    { path: 'signup', redirectTo: 'signup/patient' },
    { path: '**', redirectTo: 'not-found' }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes, { useHash: true })],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;


/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var animations_1 = __webpack_require__("../../../platform-browser/esm5/animations.js");
var animations_2 = __webpack_require__("../../../platform-browser/esm5/animations.js");
var table_1 = __webpack_require__("../../../cdk/esm5/table.es5.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var core_2 = __webpack_require__("../../../../@ngx-translate/core/index.js");
var http_loader_1 = __webpack_require__("../../../../@ngx-translate/http-loader/index.js");
__webpack_require__("../../../../hammerjs/hammer.js");
var ngx_wow_1 = __webpack_require__("../../../../ngx-wow/index.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var app_routing_module_1 = __webpack_require__("../../../../../src/app/app-routing.module.ts");
var shared_1 = __webpack_require__("../../../../../src/app/shared/index.ts");
var loginsignauth_guard_1 = __webpack_require__("../../../../../src/app/shared/guard/loginsignauth.guard.ts");
var category_module_1 = __webpack_require__("../../../../../src/app/selection-page/category.module.ts");
var dentist_login_module_1 = __webpack_require__("../../../../../src/app/dentist-login/dentist-login.module.ts");
// AoT requires an exported function for factories
function createTranslateLoader(http) {
    // for development
    // return new TranslateHttpLoader(http, '/start-angular/SB-Admin-BS4-Angular-5/master/dist/assets/i18n/', '.json');
    return new http_loader_1.TranslateHttpLoader(http, './assets/i18n/', '.json');
}
exports.createTranslateLoader = createTranslateLoader;
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                animations_2.NoopAnimationsModule,
                http_1.HttpClientModule,
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: createTranslateLoader,
                        deps: [http_1.HttpClient]
                    }
                }),
                app_routing_module_1.AppRoutingModule,
                ngx_wow_1.NgwWowModule.forRoot(),
                forms_1.FormsModule,
                category_module_1.CategoryModule,
                dentist_login_module_1.DentistLoginModule,
                table_1.CdkTableModule
            ],
            declarations: [app_component_1.AppComponent],
            providers: [shared_1.AuthGuard, loginsignauth_guard_1.DashAuthGuard],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/dentist-login/dentist-login-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var dentist_login_component_1 = __webpack_require__("../../../../../src/app/dentist-login/dentist-login.component.ts");
var routes = [
    {
        path: '',
        component: dentist_login_component_1.DentistLoginComponent
    }
];
var DentistLoginRoutingModule = (function () {
    function DentistLoginRoutingModule() {
    }
    DentistLoginRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], DentistLoginRoutingModule);
    return DentistLoginRoutingModule;
}());
exports.DentistLoginRoutingModule = DentistLoginRoutingModule;


/***/ }),

/***/ "../../../../../src/app/dentist-login/dentist-login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"jumbotron vertical-center\">\n    <div class=\"wow fadeIn container align-middle justify-content-center\" [@routerTransition]>\n        <ngx-loading [show]=\"loading\"></ngx-loading>\n        <div class=\"row login-sec\">\n            <div class=\"col-md-4 col-sm-4\">\n                <h2 class=\"text-center\">Dentist Login</h2>\n                <form [formGroup]=\"loginForm\" style=\"margin-left: 20%;margin-right:15%;width: 70%;\">\n                    <div class=\"form-content\">\n                        <div class=\"form-group\">\n                            <mat-form-field>\n                                <input matInput placeholder=\"Enter your Unique Id\" formControlName=\"uniqueId\" required>\n                                <mat-error *ngIf=\"loginForm.get('uniqueId').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <div class=\"form-content\">\n                        <div class=\"form-group\">\n                            <mat-form-field>\n                                <input matInput placeholder=\"Enter Username\" formControlName=\"username\" required>\n                                <mat-error *ngIf=\"loginForm.get('username').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <div class=\"form-content\">\n                        <div class=\"form-group\">\n                            <mat-form-field>\n                                <input matInput placeholder=\"Enter your password\" formControlName=\"password\" required>\n                                <mat-error *ngIf=\"loginForm.get('password').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <div class=\"row\">\n                        <div class=\"col-md-5 text-center\">\n                            <button class=\"btn btn-login\" type=\"submit\" (click)=\"onLoggedin()\" [disabled]=\"!loginForm.valid\"> Login\n                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>\n                            </button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n            <div class=\"d-none col-md-8 col-sm-8 thumbnail\">\n                <img class=\"img-fluid img-thumbnail\" src=\"https://expertbeacon.com/sites/default/files/pain-free_dentistry_can_help_you_get_over_your_fear_of_the_dentist.jpg\"\n                    alt=\"First slide\">\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/dentist-login/dentist-login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@import url(\"//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css\");\n.login-block-1 {\n  background: #33691e;\n  /* fallback for old browsers */\n  /* Chrome 10-25, Safari 5.1-6 */\n  background: -webkit-gradient(linear, left top, left bottom, from(#c8e6c9), to(#33691e));\n  background: linear-gradient(to bottom, #c8e6c9, #33691e);\n  /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n  float: left;\n  width: 100%;\n  padding: 50px 0; }\n.banner-sec {\n  background-size: cover;\n  min-height: 500px;\n  border-radius: 0 10px 10px 0;\n  padding: 0; }\n.container {\n  background: #fff;\n  border-radius: 10px;\n  -webkit-box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1);\n          box-shadow: 15px 20px 0px rgba(0, 0, 0, 0.1); }\n.carousel-inner {\n  border-radius: 0 10px 10px 0; }\n.carousel-caption {\n  text-align: left;\n  left: 5%; }\n.jumbotron.vertical-center {\n  margin-bottom: 0; }\n.vertical-center {\n  min-height: 100%;\n  min-height: 100vh;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -ms-flex-align: center;\n  align-items: center;\n  width: 100%;\n  -webkit-box-pack: center;\n  -ms-flex-pack: center;\n  justify-content: center; }\n.login-sec {\n  background: -webkit-gradient(linear, left top, left bottom, from(#c8e6c9), to(#33691e));\n  background: linear-gradient(to bottom, #c8e6c9, #33691e); }\n.login-sec .copy-text {\n    font-size: 13px;\n    text-align: center; }\n.login-sec .copy-text i {\n      color: #FEB58A; }\n.login-sec .copy-text a {\n      color: #E36262; }\n.login-sec h2 {\n    margin-top: 15%;\n    margin-bottom: 15px;\n    font-weight: 800;\n    font-size: 30px;\n    color: #DE6262; }\n.login-sec h2:after {\n      content: \" \";\n      width: 100px;\n      height: 5px;\n      background: #FEB58A;\n      display: block;\n      margin-left: auto;\n      margin-right: auto; }\n.btn-login {\n  background: #33691e;\n  color: #fff;\n  font-weight: 600; }\n.banner-text {\n  width: 70%;\n  position: absolute;\n  bottom: 40px;\n  padding-left: 20px; }\n.banner-text h2 {\n    color: #fff;\n    font-weight: 600; }\n.banner-text h2:after {\n      content: \" \";\n      width: 100px;\n      height: 5px;\n      background: #FFF;\n      display: block;\n      margin-top: 20px;\n      border-radius: 3px; }\n.banner-text p {\n    color: #fff; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/dentist-login/dentist-login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var wow_min_1 = __webpack_require__("../../../../wowjs/dist/wow.min.js");
var router_animations_1 = __webpack_require__("../../../../../src/app/router.animations.ts");
var DentistLoginComponent = (function () {
    function DentistLoginComponent(fb) {
        this.fb = fb;
        this.loading = false;
    }
    DentistLoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this.fb.group({
            uniqueId: new forms_1.FormControl('', [forms_1.Validators.required]),
            username: new forms_1.FormControl('', [forms_1.Validators.required]),
            password: new forms_1.FormControl('', [forms_1.Validators.required])
        });
    };
    DentistLoginComponent.prototype.ngAfterViewInit = function () {
        new wow_min_1.WOW().init();
    };
    DentistLoginComponent.prototype.onLoggedin = function () {
        console.log(this.loginForm.value);
        localStorage.setItem('isLoggedin', 'true');
        console.log("hello" + localStorage.getItem('isLoggedin'));
        this.loading = true;
        this.loginForm.reset();
    };
    DentistLoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/dentist-login/dentist-login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/dentist-login/dentist-login.component.scss")],
            animations: [router_animations_1.routerTransition()]
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder])
    ], DentistLoginComponent);
    return DentistLoginComponent;
}());
exports.DentistLoginComponent = DentistLoginComponent;


/***/ }),

/***/ "../../../../../src/app/dentist-login/dentist-login.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var ngx_loading_1 = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
var dentist_login_routing_module_1 = __webpack_require__("../../../../../src/app/dentist-login/dentist-login-routing.module.ts");
var dentist_login_component_1 = __webpack_require__("../../../../../src/app/dentist-login/dentist-login.component.ts");
var DentistLoginModule = (function () {
    function DentistLoginModule() {
    }
    DentistLoginModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, material_1.MatInputModule,
                material_1.MatSelectModule,
                material_1.MatFormFieldModule,
                material_1.MatIconModule,
                forms_1.ReactiveFormsModule,
                dentist_login_routing_module_1.DentistLoginRoutingModule, forms_1.FormsModule, ngx_loading_1.LoadingModule.forRoot({
                    animationType: ngx_loading_1.ANIMATION_TYPES.rectangleBounce,
                    backdropBackgroundColour: 'rgba(0,0,0,0.5)',
                    backdropBorderRadius: '4px',
                    primaryColour: '#ffffff',
                    secondaryColour: '#ffffff',
                    tertiaryColour: '#ffffff'
                })],
            declarations: [dentist_login_component_1.DentistLoginComponent]
        })
    ], DentistLoginModule);
    return DentistLoginModule;
}());
exports.DentistLoginModule = DentistLoginModule;


/***/ }),

/***/ "../../../../../src/app/router.animations.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = __webpack_require__("../../../animations/esm5/animations.js");
function routerTransition() {
    return slideToTop();
}
exports.routerTransition = routerTransition;
function slideToRight() {
    return animations_1.trigger('routerTransition', [
        animations_1.state('void', animations_1.style({})),
        animations_1.state('*', animations_1.style({})),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateX(-100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateX(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(100%)' }))
        ])
    ]);
}
exports.slideToRight = slideToRight;
function slideToLeft() {
    return animations_1.trigger('routerTransition', [
        animations_1.state('void', animations_1.style({})),
        animations_1.state('*', animations_1.style({})),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateX(100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateX(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(-100%)' }))
        ])
    ]);
}
exports.slideToLeft = slideToLeft;
function slideToBottom() {
    return animations_1.trigger('routerTransition', [
        animations_1.state('void', animations_1.style({})),
        animations_1.state('*', animations_1.style({})),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateY(-100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateY(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(100%)' }))
        ])
    ]);
}
exports.slideToBottom = slideToBottom;
function slideToTop() {
    return animations_1.trigger('routerTransition', [
        animations_1.state('void', animations_1.style({})),
        animations_1.state('*', animations_1.style({})),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateY(100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateY(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(-100%)' }))
        ])
    ]);
}
exports.slideToTop = slideToTop;


/***/ }),

/***/ "../../../../../src/app/selection-page/category-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var category_component_1 = __webpack_require__("../../../../../src/app/selection-page/category.component.ts");
var routes = [
    {
        path: '',
        component: category_component_1.CategoryComponent
    }
];
var CategoryRoutingModule = (function () {
    function CategoryRoutingModule() {
    }
    CategoryRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], CategoryRoutingModule);
    return CategoryRoutingModule;
}());
exports.CategoryRoutingModule = CategoryRoutingModule;


/***/ }),

/***/ "../../../../../src/app/selection-page/category.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark fixed-top\">\n    <div class=\"container\">\n        <a class=\"navbar-brand\" href=\"#\">Dental</a>\n        <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\"\n            aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n            Menu\n            <i class=\"fa fa-bars\"></i>\n        </button>\n        <div class=\"collapse navbar-collapse\" id=\"navbarResponsive\">\n            <ul class=\"navbar-nav ml-auto\">\n                <li class=\"nav-item\">\n                    <a class=\"nav-link js-scroll-trigger\" href=\"#services\">\n                        <i class=\"fa fa-globe\"></i> Services </a>\n                </li>\n                <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link js-scroll-trigger\" href=\"#portfolio\">\n                        <i class=\"fa fa-image\"></i> Portfolio </a>\n                </li>\n                <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link js-scroll-trigger\" href=\"#about\">\n                        <i class=\"fa fa-exclamation\"></i> About </a>\n                </li>\n                <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item\">\n                        <a class=\"nav-link js-scroll-trigger\" href=\"#contact\">\n                            <i class=\"fa fa-envelope\"></i> Contact </a>\n                    </li>\n                    <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link\" [routerLink]=\"['/login/patient']\">\n                        <i class=\"fa fa-user\"></i> Patient Sign In</a>\n                </li>\n                <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link\" [routerLink]=\"['/login/dentist']\">\n                        <i class=\"fa fa-unlock-alt\"></i> Admin Sign In</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n</nav>\n<!-- Header with Background Image\n@mixin serif-font {\n  font-family: 'Droid Serif', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n}\n@mixin script-font {\n  font-family: 'Kaushan Script', 'Helvetica Neue', Helvetica, Arial, cursive;\n}\n@mixin body-font {\n  font-family: 'Roboto Slab', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n}\n@mixin heading-font {\n  font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n} -->\n<ngb-carousel [interval]=\"4000\">\n    <ng-template ngbSlide class=\"animated slideInRight\" *ngFor=\"let slider of sliders\">\n        <div class=\"background\">\n            <img class=\"animated fadeIn img-fluid img-responsive mx-auto d-block image\" [src]=\"slider.imagePath\" alt=\"Random first slide\" width=\"100%\">\n            <div class=\"layer\">\n                <div class=\"animated slideInRight carousel-caption\" style=\"padding-bottom:100px\">\n                    <h1 style=\"font-size:5vw;font-family: 'Kaushan Script', 'Helvetica Neue', Helvetica, Arial, cursive;\">{{slider.label}}</h1>\n                    <h4 style=\"font-family: 'Roboto Slab', 'Helvetica Neue', Helvetica, Arial, sans-serif;\">{{slider.text}}</h4>\n                </div>\n            </div>\n        </div>\n    </ng-template>\n</ngb-carousel>\n<section id=\"services\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 text-center wow fadeIn\">\n                <h1 class=\"section-heading text-uppercase\">Services</h1>\n                <h5 class=\"section-subheading text-muted\">Explore the general and specialty dentistry we offer you and your family.</h5>\n            </div>\n        </div>\n        <div class=\"row text-center\">\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-users fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">General Dentistry</h4>\n                <p class=\"text-muted text-justify\">Our Dental Associates dentists and their teams are passionate about providing comprehensive, quality dental\n                    care to patients of all ages. Maintaining good oral health by seeing your dentist and hygienist twice\n                    a year assures you enjoy life with a healthy smile and greatly reduces your risk of problems arising.</p>\n            </div>\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-certificate fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">Cosmetic Dentistry</h4>\n                <p class=\"text-muted text-justify\">Our team at Dental Associates wants you to feel more confident about your smile. We offer many cosmetic dentistry\n                    choices, and will work with you to create the smile you want.</p>\n            </div>\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-child fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">pediatric Dentistry</h4>\n                <p class=\"text-muted text-justify\">Your child deserves the best dental care possible. At Dental Associates, our caring pediatric dentists and\n                    their teams make your child feel comfortable and help prepare them for a lifetime of healthy smiles.\n                    Not only do we help teach your child how to take care of their teeth, but we also have the benefit of\n                    in-house orthodontists.</p>\n            </div>\n        </div>\n        <div class=\"row text-center\">\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-link fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">Braces & Orthodontics</h4>\n                <p class=\"text-muted text-justify\">Dental Associates clinics offer the benefit of an orthodontist right down the hall from your family’s general\n                    and pediatric dentists. When your child is ready for braces, they’ll be able to receive braces care in\n                    the same clinic where they receive the rest of their dental care. </p>\n            </div>\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-steam fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">Endodontics & Root Canals</h4>\n                <p class=\"text-muted text-justify\">Endodontics is the specialty of dentistry that focuses on the treatment of the pulp and roots of the teeth.\n                    Since the area inside and underneath your teeth is very complex, an infection in the roots of the teeth\n                    can be very painful. Therefore, it’s important to maintain proper oral hygiene habits and keep the surface\n                    of your teeth healthy.</p>\n            </div>\n            <div class=\"col-md-4 wow bounceIn\">\n                <span class=\"fa-stack fa-4x\">\n                    <i class=\"fa fa-circle fa-stack-2x text-primary\"></i>\n                    <i class=\"fa fa-american-sign-language-interpreting fa-stack-1x fa-inverse\"></i>\n                </span>\n                <h4 class=\"service-heading\">Oral Surgery</h4>\n                <p class=\"text-muted text-justify\">Our oral surgeons at Dental Associates are expertly-trained dentists who specialize in surgical procedures\n                    involving your teeth, mouth and jaw. We offer oral surgery procedures to correct various issues, and\n                    our dentists and surgeons will work together to provide the best treatment plan for your individual case.</p>\n            </div>\n        </div>\n    </div>\n</section>\n<!-- Page Content -->\n<section class=\"bg-light portfolio\" id=\"portfolio\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 text-center\">\n                <h2 class=\"section-heading text-uppercase wow bounceIn\">Portfolio</h2>\n            </div>\n        </div>\n        <br>\n        <div class=\"row\">\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInLeft\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal1\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"https://dentmedico.cz/wp-content/uploads/2016/04/endo.jpg\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Endodontics</h4>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInDown\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal2\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"https://i.pinimg.com/originals/b8/4b/ee/b84beec5f5881a12f7eed4ae2e2163b2.png\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Oral and Maxillofacial Radiology</h4>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInRight\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal3\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"http://atlasdental.com/wp-content/uploads/2018/02/TeethWhitening_beforeafter2.jpg\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Orthodontics</h4>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInLeft\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal4\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"http://azphp.com/wp-content/uploads/2017/01/dental-patient-examininggums.jpg\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Periodontics</h4>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInDown\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal5\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"https://www.onlinemphdegree.net/files/2013/07/public-health-dentist.jpg\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Dental Public Health</h4>\n                </div>\n            </div>\n            <div class=\"col-md-4 col-sm-6 portfolio-item wow bounceInRight\">\n                <a class=\"portfolio-link\" data-toggle=\"modal\" href=\"#portfolioModal6\">\n                    <div class=\"portfolio-hover\">\n                        <div class=\"portfolio-hover-content\">\n                            <i class=\"fa fa-plus fa-3x\"></i>\n                        </div>\n                    </div>\n                    <img class=\"img-fluid\" src=\"http://www.pasqualoms.com/files/2012/12/86F8E15E-0F0A-48A5-9E3B-4E8EA6E0B601.jpg\" alt=\"\">\n                </a>\n                <div class=\"portfolio-caption\">\n                    <h4>Oral and Maxillofacial Surgery</h4>\n                </div>\n            </div>\n        </div>\n    </div>\n</section>\n\n<section class=\"about\" id=\"about\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 text-center\">\n                <h2 class=\"section-heading text-uppercase wow bounceIn\">About</h2>\n                <h3 class=\"section-subheading text-muted\">Lorem ipsum dolor sit amet consectetur.</h3>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-lg-12\">\n                <ul class=\"timeline\" style=\"list-style:none\">\n                    <li>\n                        <div class=\"timeline-image wow slideInRight\">\n                            <img class=\"rounded-circle img-fluid\" src=\"https://www.countrysidedentist.com/assets/images/interior-technology.jpg\" alt=\"\">\n                        </div>\n                        <div class=\"timeline-panel wow slideInLeft\">\n                            <div class=\"timeline-heading\">\n                                <h4>2009-2011</h4>\n                                <h4 class=\"subheading\">Our Humble Beginnings</h4>\n                            </div>\n                            <div class=\"timeline-body\">\n                                <p class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente,\n                                    totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et\n                                    ea quo dolore laudantium consectetur!</p>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"timeline-inverted\">\n                        <div class=\"timeline-image wow slideInLeft\">\n                            <img class=\"rounded-circle img-fluid\" src=\"http://donnasmortgages.com/wp-content/uploads/2016/07/Dental-Financing-300x300.jpg\"\n                                alt=\"\">\n                        </div>\n                        <div class=\"timeline-panel wow slideInRight\">\n                            <div class=\"timeline-heading\">\n                                <h4>March 2011</h4>\n                                <h4 class=\"subheading\">An Agency is Born</h4>\n                            </div>\n                            <div class=\"timeline-body\">\n                                <p class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente,\n                                    totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et\n                                    ea quo dolore laudantium consectetur!</p>\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class=\"timeline-image wow slideInRight\">\n                            <img class=\"rounded-circle img-fluid\" src=\"http://www.dentalseasons.com.au/img/services/cosmetic-dentistry.png\" alt=\"\">\n                        </div>\n                        <div class=\"timeline-panel wow slideInLeft\">\n                            <div class=\"timeline-heading\">\n                                <h4>December 2012</h4>\n                                <h4 class=\"subheading\">Transition to Full Service</h4>\n                            </div>\n                            <div class=\"timeline-body\">\n                                <p class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente,\n                                    totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et\n                                    ea quo dolore laudantium consectetur!</p>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"timeline-inverted wow bounceInDown\">\n                        <div class=\"timeline-image\">\n                            <h4>Be Part\n                                <br>Of Our\n                                <br>Story!</h4>\n                        </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </div>\n</section>\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal1\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"https://dentmedico.cz/wp-content/uploads/2016/04/endo.jpg\" alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Threads</li>\n                                <li>Category: Illustration</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- Modal 2 -->\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal2\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"https://i.pinimg.com/originals/b8/4b/ee/b84beec5f5881a12f7eed4ae2e2163b2.png\"\n                                alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Explore</li>\n                                <li>Category: Graphic Design</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- Modal 3 -->\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal3\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"http://atlasdental.com/wp-content/uploads/2018/02/TeethWhitening_beforeafter2.jpg\"\n                                alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Finish</li>\n                                <li>Category: Identity</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- Modal 4 -->\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal4\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"http://azphp.com/wp-content/uploads/2017/01/dental-patient-examininggums.jpg\"\n                                alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Lines</li>\n                                <li>Category: Branding</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- Modal 5 -->\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal5\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"https://www.onlinemphdegree.net/files/2013/07/public-health-dentist.jpg\" alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Southwest</li>\n                                <li>Category: Website Design</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- Modal 6 -->\n<div class=\"portfolio-modal modal fade\" id=\"portfolioModal6\" tabindex=\"-1\" role=\"dialog\" aria-hidden=\"true\">\n    <div class=\"modal-dialog\">\n        <div class=\"modal-content\">\n            <div class=\"close-modal\" data-dismiss=\"modal\">\n                <div class=\"lr\">\n                    <div class=\"rl\"></div>\n                </div>\n            </div>\n            <div class=\"container\">\n                <div class=\"row\">\n                    <div class=\"col-lg-8 mx-auto\">\n                        <div class=\"modal-body\">\n                            <!-- Project Details Go Here -->\n                            <h2 class=\"text-uppercase\">Project Name</h2>\n                            <p class=\"item-intro text-muted\">Lorem ipsum dolor sit amet consectetur.</p>\n                            <img class=\"img-fluid d-block mx-auto\" src=\"http://www.pasqualoms.com/files/2012/12/86F8E15E-0F0A-48A5-9E3B-4E8EA6E0B601.jpg\"\n                                alt=\"\">\n                            <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit.\n                                Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt\n                                officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>\n                            <ul class=\"list-inline\">\n                                <li>Date: January 2017</li>\n                                <li>Client: Window</li>\n                                <li>Category: Photography</li>\n                            </ul>\n                            <button class=\"btn btn-primary\" data-dismiss=\"modal\" type=\"button\">\n                                <i class=\"fa fa-times\"></i>\n                                Close Project</button>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<section class=\"contact wow fadeInUp\" id=\"contact\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"col-lg-12 text-center\">\n                <h2 class=\"section-heading text-uppercase\">Contact Us</h2>\n                <h3 class=\"section-subheading text-muted\">Lorem ipsum dolor sit amet consectetur.</h3>\n            </div>\n        </div>\n        <div class=\"row\">\n            <div class=\"col-lg-12\">\n                <form id=\"contactForm\" name=\"sentMessage\" novalidate>\n                    <div class=\"row\">\n                        <div class=\"col-md-6\">\n                            <div class=\"form-group\">\n                                <input class=\"form-control\" id=\"name\" type=\"text\" placeholder=\"Your Name *\" required data-validation-required-message=\"Please enter your name.\">\n                                <p class=\"help-block text-danger\"></p>\n                            </div>\n                            <div class=\"form-group\">\n                                <input class=\"form-control\" id=\"email\" type=\"email\" placeholder=\"Your Email *\" required data-validation-required-message=\"Please enter your email address.\">\n                                <p class=\"help-block text-danger\"></p>\n                            </div>\n                            <div class=\"form-group\">\n                                <input class=\"form-control\" id=\"phone\" type=\"tel\" placeholder=\"Your Phone *\" required data-validation-required-message=\"Please enter your phone number.\">\n                                <p class=\"help-block text-danger\"></p>\n                            </div>\n                        </div>\n                        <div class=\"col-md-6\">\n                            <div class=\"form-group\">\n                                <textarea class=\"form-control\" id=\"message\" placeholder=\"Your Message *\" required data-validation-required-message=\"Please enter a message.\"></textarea>\n                                <p class=\"help-block text-danger\"></p>\n                            </div>\n                        </div>\n                        <div class=\"clearfix\"></div>\n                        <div class=\"col-lg-12 text-center\">\n                            <div id=\"success\"></div>\n                            <button id=\"sendMessageButton\" class=\"btn btn-primary btn-xl text-uppercase\" type=\"submit\">Send Message</button>\n                        </div>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</section>\n\n<!-- Footer -->\n<footer class=\"py-5 bg-dark\">\n    <div class=\"container\">\n        <p class=\"m-0 text-center text-white\">Copyright &copy; Your Website 2018</p>\n    </div>\n    <!-- /.container -->\n</footer>"

/***/ }),

/***/ "../../../../../src/app/selection-page/category.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".navbar .navbar-toggler {\n  font-size: 12px;\n  right: 0;\n  padding: 13px;\n  text-transform: uppercase;\n  color: #000000;\n  border: 0;\n  background-color: yellow;\n  font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\n.navbar .navbar-brand {\n  color: yellow;\n  font-family: 'Kaushan Script', 'Helvetica Neue', Helvetica, Arial, cursive; }\n\n.navbar .navbar-brand.active, .navbar .navbar-brand:active, .navbar .navbar-brand:focus, .navbar .navbar-brand:hover {\n    color: #cccc00; }\n\n.navbar .navbar-nav .nav-item .nav-link {\n  font-size: 90%;\n  font-weight: 400;\n  padding: 0.75em 0;\n  letter-spacing: 1px;\n  color: white; }\n\n.navbar .navbar-nav .nav-item .nav-link.active, .navbar .navbar-nav .nav-item .nav-link:hover {\n    color: yellow; }\n\n@media (min-width: 100px) {\n  .navbar {\n    padding-top: 25px;\n    padding-bottom: 25px;\n    -webkit-transition: padding-top 0.3s, padding-bottom 0.3s;\n    transition: padding-top 0.3s, padding-bottom 0.3s;\n    border: none;\n    background-color: transparent; }\n    .navbar .navbar-brand {\n      font-size: 1.75em;\n      -webkit-transition: all 0.3s;\n      transition: all 0.3s; }\n    .navbar .navbar-nav .nav-item .nav-link {\n      padding: 1.1em 1em !important; }\n    .navbar.navbar-shrink {\n      padding-top: 0;\n      padding-bottom: 0;\n      background-color: #000000; }\n      .navbar.navbar-shrink .navbar-brand {\n        font-size: 1.25em;\n        padding: 12px 0; }\n  .scrolled {\n    background: #000000;\n    padding-top: 10px;\n    padding-bottom: 10px;\n    -webkit-transition: padding-top 0.3s, padding-bottom 0.3s;\n    transition: padding-top 0.3s, padding-bottom 0.3s; }\n    .scrolled .navbar-brand {\n      font-size: 1.4em;\n      -webkit-transition: all 0.3s;\n      transition: all 0.3s; }\n    .scrolled .navbar-nav .nav-item .nav-link {\n      padding: 0.5em 0.5em !important; }\n    .scrolled.navbar-shrink {\n      padding-top: 0;\n      padding-bottom: 0;\n      background-color: #000000; }\n      .scrolled.navbar-shrink .navbar-brand {\n        font-size: 1.25em;\n        padding: 12px 0; } }\n\n.service-heading {\n  margin: 15px 0;\n  text-transform: none;\n  color: rgba(27, 46, 84, 0.8);\n  font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\n.section-heading {\n  margin: 15px 0;\n  text-transform: none;\n  color: #cccc00;\n  font-family: 'Roboto Slab', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-weight: 900; }\n\n.portfolio .portfolio-item {\n  right: 0;\n  margin: 0 0 15px; }\n\n.portfolio .portfolio-item .portfolio-link {\n    position: relative;\n    display: block;\n    max-width: 400px;\n    margin: 0 auto;\n    cursor: pointer; }\n\n.portfolio .portfolio-item .portfolio-link .portfolio-hover {\n      position: absolute;\n      width: 100%;\n      height: 100%;\n      -webkit-transition: all ease 0.5s;\n      transition: all ease 0.5s;\n      opacity: 0;\n      background: rgba(254, 209, 54, 0.9); }\n\n.portfolio .portfolio-item .portfolio-link .portfolio-hover:hover {\n        opacity: 1; }\n\n.portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content {\n        font-size: 20px;\n        position: absolute;\n        top: 50%;\n        width: 100%;\n        height: 20px;\n        margin-top: -12px;\n        text-align: center;\n        color: white; }\n\n.portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content i {\n          margin-top: -12px; }\n\n.portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content h3,\n        .portfolio .portfolio-item .portfolio-link .portfolio-hover .portfolio-hover-content h4 {\n          margin: 0; }\n\n.portfolio .portfolio-item .portfolio-caption {\n    max-width: 400px;\n    margin: 0 auto;\n    padding: 25px;\n    text-align: center;\n    background-color: #fff; }\n\n.portfolio .portfolio-item .portfolio-caption h4 {\n      margin: 0;\n      text-transform: none; }\n\n.portfolio .portfolio-item .portfolio-caption p {\n      font-size: 16px;\n      font-style: italic;\n      margin: 0;\n      font-family: 'Droid Serif', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\n.portfolio * {\n  z-index: 2; }\n\n@media (min-width: 767px) {\n  .portfolio .portfolio-item {\n    margin: 0 0 30px; } }\n\n.portfolio-modal {\n  padding-right: 0px !important; }\n\n.portfolio-modal .modal-dialog {\n    margin: 1rem;\n    max-width: 100vw; }\n\n.portfolio-modal .modal-content {\n    padding: 100px 0;\n    text-align: center; }\n\n.portfolio-modal .modal-content h2 {\n      font-size: 3em;\n      margin-bottom: 15px; }\n\n.portfolio-modal .modal-content p {\n      margin-bottom: 30px; }\n\n.portfolio-modal .modal-content p.item-intro {\n      font-size: 16px;\n      font-style: italic;\n      margin: 20px 0 30px;\n      font-family: 'Droid Serif', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\n.portfolio-modal .modal-content ul.list-inline {\n      margin-top: 0;\n      margin-bottom: 30px; }\n\n.portfolio-modal .modal-content img {\n      margin-bottom: 30px; }\n\n.portfolio-modal .modal-content button {\n      cursor: pointer; }\n\n.portfolio-modal .close-modal {\n    position: absolute;\n    top: 25px;\n    right: 25px;\n    width: 75px;\n    height: 75px;\n    cursor: pointer;\n    background-color: transparent; }\n\n.portfolio-modal .close-modal:hover {\n      opacity: 0.3; }\n\n.portfolio-modal .close-modal .lr {\n      /* Safari and Chrome */\n      z-index: 1051;\n      width: 1px;\n      height: 75px;\n      margin-left: 35px;\n      /* IE 9 */\n      -webkit-transform: rotate(45deg);\n      transform: rotate(45deg);\n      background-color: #212529; }\n\n.portfolio-modal .close-modal .lr .rl {\n        /* Safari and Chrome */\n        z-index: 1052;\n        width: 1px;\n        height: 75px;\n        /* IE 9 */\n        -webkit-transform: rotate(90deg);\n        transform: rotate(90deg);\n        background-color: #212529; }\n\nngb-carousel {\n  height: 652px;\n  width: 100%; }\n\n.background {\n  position: relative;\n  height: 652px; }\n\n.image {\n  height: 652px; }\n\n.layer {\n  background-color: rgba(0, 0, 0, 0.55);\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%; }\n\n.business-header {\n  height: 50vh;\n  min-height: 300px;\n  background: url(\"http://www.orthodonticmasters.com/images/a1111.jpg\") center center no-repeat scroll;\n  background-size: cover;\n  -o-background-size: cover; }\n\n.card {\n  height: 100%; }\n\nbody {\n  overflow-x: hidden;\n  font-family: 'Roboto Slab', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\np {\n  line-height: 1.75; }\n\na {\n  color: #fed136; }\n\na:hover {\n    color: #fec503; }\n\n.text-primary {\n  color: #fed136 !important; }\n\nh1,\nh2,\nh3,\nh4,\nh5,\nh6 {\n  font-weight: 700;\n  font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\nsection {\n  padding: 70px 0; }\n\nsection h2.section-heading {\n    font-size: 40px;\n    margin-bottom: 15px; }\n\nsection h3.section-subheading {\n    font-size: 16px;\n    font-weight: 400;\n    font-style: italic;\n    margin-bottom: 75px;\n    text-transform: none;\n    font-family: 'Droid Serif', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\n@media (min-width: 768px) {\n  section {\n    padding: 100px 0; } }\n\n.btn {\n  font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif;\n  font-weight: 700; }\n\n.btn-xl {\n  font-size: 18px;\n  padding: 20px 40px; }\n\n.btn-primary {\n  background-color: #fed136;\n  border-color: #fed136; }\n\n.btn-primary:active, .btn-primary:focus, .btn-primary:hover {\n    background-color: #fec810 !important;\n    border-color: #fec810 !important;\n    color: white; }\n\n.btn-primary:active, .btn-primary:focus {\n    -webkit-box-shadow: 0 0 0 0.2rem rgba(254, 209, 55, 0.5) !important;\n            box-shadow: 0 0 0 0.2rem rgba(254, 209, 55, 0.5) !important; }\n\n::-moz-selection {\n  background: #fed136;\n  text-shadow: none; }\n\n::selection {\n  background: #fed136;\n  text-shadow: none; }\n\nimg::-moz-selection {\n  background: transparent; }\n\nimg::selection {\n  background: transparent; }\n\nimg::-moz-selection {\n  background: transparent; }\n\n.timeline {\n  position: relative;\n  padding: 0;\n  list-style: none; }\n\n.timeline:before {\n    position: absolute;\n    top: 0;\n    bottom: 0;\n    left: 40px;\n    width: 2px;\n    margin-left: -1.5px;\n    content: '';\n    background-color: #e9ecef; }\n\n.timeline > li {\n    position: relative;\n    min-height: 50px;\n    margin-bottom: 50px; }\n\n.timeline > li:after, .timeline > li:before {\n      display: table;\n      content: ' '; }\n\n.timeline > li:after {\n      clear: both; }\n\n.timeline > li .timeline-panel {\n      position: relative;\n      float: right;\n      width: 100%;\n      padding: 0 20px 0 100px;\n      text-align: left; }\n\n.timeline > li .timeline-panel:before {\n        right: auto;\n        left: -15px;\n        border-right-width: 15px;\n        border-left-width: 0; }\n\n.timeline > li .timeline-panel:after {\n        right: auto;\n        left: -14px;\n        border-right-width: 14px;\n        border-left-width: 0; }\n\n.timeline > li .timeline-image {\n      position: absolute;\n      z-index: 100;\n      left: 0;\n      width: 80px;\n      height: 80px;\n      margin-left: 0;\n      text-align: center;\n      color: white;\n      border: 7px solid #e9ecef;\n      border-radius: 100%;\n      background-color: #fed136; }\n\n.timeline > li .timeline-image h4 {\n        font-size: 10px;\n        line-height: 14px;\n        margin-top: 12px; }\n\n.timeline > li.timeline-inverted > .timeline-panel {\n      float: right;\n      padding: 0 20px 0 100px;\n      text-align: left; }\n\n.timeline > li.timeline-inverted > .timeline-panel:before {\n        right: auto;\n        left: -15px;\n        border-right-width: 15px;\n        border-left-width: 0; }\n\n.timeline > li.timeline-inverted > .timeline-panel:after {\n        right: auto;\n        left: -14px;\n        border-right-width: 14px;\n        border-left-width: 0; }\n\n.timeline > li:last-child {\n      margin-bottom: 0; }\n\n.timeline .timeline-heading h4 {\n    margin-top: 0;\n    color: inherit; }\n\n.timeline .timeline-heading h4.subheading {\n      text-transform: none; }\n\n.timeline .timeline-body > ul,\n  .timeline .timeline-body > p {\n    margin-bottom: 0; }\n\n@media (min-width: 768px) {\n  .timeline:before {\n    left: 50%; }\n  .timeline > li {\n    min-height: 100px;\n    margin-bottom: 100px; }\n    .timeline > li .timeline-panel {\n      float: left;\n      width: 41%;\n      padding: 0 20px 20px 30px;\n      text-align: right; }\n    .timeline > li .timeline-image {\n      left: 50%;\n      width: 100px;\n      height: 100px;\n      margin-left: -50px; }\n      .timeline > li .timeline-image h4 {\n        font-size: 13px;\n        line-height: 18px;\n        margin-top: 16px; }\n    .timeline > li.timeline-inverted > .timeline-panel {\n      float: right;\n      padding: 0 30px 20px 20px;\n      text-align: left; } }\n\n@media (min-width: 992px) {\n  .timeline > li {\n    min-height: 150px; }\n    .timeline > li .timeline-panel {\n      padding: 0 20px 20px; }\n    .timeline > li .timeline-image {\n      width: 150px;\n      height: 150px;\n      margin-left: -75px; }\n      .timeline > li .timeline-image h4 {\n        font-size: 18px;\n        line-height: 26px;\n        margin-top: 30px; }\n    .timeline > li.timeline-inverted > .timeline-panel {\n      padding: 0 20px 20px; } }\n\n@media (min-width: 1200px) {\n  .timeline > li {\n    min-height: 170px; }\n    .timeline > li .timeline-panel {\n      padding: 0 20px 20px 100px; }\n    .timeline > li .timeline-image {\n      width: 170px;\n      height: 170px;\n      margin-left: -85px; }\n      .timeline > li .timeline-image h4 {\n        margin-top: 40px; }\n    .timeline > li.timeline-inverted > .timeline-panel {\n      padding: 0 100px 20px 20px; } }\n\nsection.contact {\n  background-color: #212529;\n  background-repeat: no-repeat;\n  background-position: center; }\n\nsection.contact .section-heading {\n    color: #fff; }\n\nsection.contact .form-group {\n    margin-bottom: 25px; }\n\nsection.contact .form-group input,\n    section.contact .form-group textarea {\n      padding: 20px; }\n\nsection.contact .form-group input.form-control {\n      height: auto; }\n\nsection.contact .form-group textarea.form-control {\n      height: 248px; }\n\nsection.contact .form-control:focus {\n    border-color: #fed136;\n    -webkit-box-shadow: none;\n            box-shadow: none; }\n\nsection.contact ::-webkit-input-placeholder {\n    font-weight: 700;\n    color: #ced4da;\n    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\nsection.contact :-moz-placeholder {\n    font-weight: 700;\n    color: #ced4da;\n    /* Firefox 18- */\n    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\nsection.contact ::-moz-placeholder {\n    font-weight: 700;\n    color: #ced4da;\n    /* Firefox 19+ */\n    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n\nsection.contact :-ms-input-placeholder {\n    font-weight: 700;\n    color: #ced4da;\n    font-family: 'Montserrat', 'Helvetica Neue', Helvetica, Arial, sans-serif; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/selection-page/category.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var wow_min_1 = __webpack_require__("../../../../wowjs/dist/wow.min.js");
var $ = __webpack_require__("../../../../jquery/dist/jquery.js");
var router_animations_1 = __webpack_require__("../../../../../src/app/router.animations.ts");
var CategoryComponent = (function () {
    function CategoryComponent(router) {
        this.router = router;
        this.sliders = [];
    }
    CategoryComponent.prototype.ngOnInit = function () {
        if ($('.navbar').length > 0) {
            $(window).on("scroll load resize", function () {
                var startY = $('.navbar').height() * 4;
                if ($(window).scrollTop() > startY) {
                    $('.navbar').addClass("scrolled");
                    $('.navbar-toggler').addClass("scrolled");
                }
                else {
                    $('.navbar').removeClass("scrolled");
                    $('.navbar-toggler').removeClass("scrolled");
                }
            });
        }
        $(document).on('click', 'a[href^="#"]', function (event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
        });
        this.sliders.push({
            imagePath: 'http://asai-dentaloffice.com/cosmetic-dentistry-aesthetic-dentist.jpg',
            label: 'First slide label',
            text: 'Nulla vitae elit libero, a pharetra augue mollis interdum.'
        }, {
            imagePath: 'http://southlondondental.com/wp-content/uploads/2017/11/gallery4.jpg',
            label: 'Second slide label',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        }, {
            imagePath: 'https://wallpapersin4k.net/wp-content/uploads/2016/12/Dentist-Smile-Wallpapers-8.jpg',
            label: 'Third slide label',
            text: 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        });
    };
    CategoryComponent.prototype.ngAfterViewInit = function () {
        new wow_min_1.WOW().init();
    };
    CategoryComponent = __decorate([
        core_1.Component({
            selector: 'app-category',
            template: __webpack_require__("../../../../../src/app/selection-page/category.component.html"),
            styles: [__webpack_require__("../../../../../src/app/selection-page/category.component.scss")],
            animations: [router_animations_1.routerTransition()]
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], CategoryComponent);
    return CategoryComponent;
}());
exports.CategoryComponent = CategoryComponent;


/***/ }),

/***/ "../../../../../src/app/selection-page/category.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var category_routing_module_1 = __webpack_require__("../../../../../src/app/selection-page/category-routing.module.ts");
var category_component_1 = __webpack_require__("../../../../../src/app/selection-page/category.component.ts");
var CategoryModule = (function () {
    function CategoryModule() {
    }
    CategoryModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                category_routing_module_1.CategoryRoutingModule,
                ng_bootstrap_1.NgbCarouselModule.forRoot(),
                ng_bootstrap_1.NgbAlertModule.forRoot(),
            ],
            declarations: [category_component_1.CategoryComponent]
        })
    ], CategoryModule);
    return CategoryModule;
}());
exports.CategoryModule = CategoryModule;


/***/ }),

/***/ "../../../../../src/app/shared/guard/auth.guard.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var Observable_1 = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
__webpack_require__("../../../../rxjs/_esm5/Rx.js");
var AuthGuard = (function () {
    function AuthGuard(router, http) {
        this.router = router;
        this.http = http;
    }
    AuthGuard.prototype.canActivate = function () {
        var _this = this;
        if (localStorage.getItem('jwtToken') == null) {
            if (localStorage.getItem('infoToken') != null) {
                this.router.navigate(['/dashboard']);
            }
            else {
                this.router.navigate(['/home']);
            }
        }
        else {
            var httpOptions = {
                headers: new http_1.HttpHeaders({ 'Authorization': localStorage.getItem('jwtToken') })
            };
            return this.http.get('/api/authenticationToken', httpOptions).map(function (data) {
                if (data[0].success == true) {
                    return true;
                }
            }).catch(function (error) {
                if (error.status === 401) {
                    if (localStorage.getItem('infoToken') != null) {
                        _this.router.navigate(['/dashboard']);
                        return Observable_1.Observable.of(false);
                    }
                    else {
                        _this.router.navigate(['/home']);
                        return Observable_1.Observable.of(false);
                    }
                }
            });
        }
    };
    AuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router, http_1.HttpClient])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;


/***/ }),

/***/ "../../../../../src/app/shared/guard/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/app/shared/guard/auth.guard.ts"));


/***/ }),

/***/ "../../../../../src/app/shared/guard/loginsignauth.guard.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var Observable_1 = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
__webpack_require__("../../../../rxjs/_esm5/Rx.js");
var DashAuthGuard = (function () {
    function DashAuthGuard(router, http) {
        this.router = router;
        this.http = http;
    }
    DashAuthGuard.prototype.canActivate = function () {
        var _this = this;
        if (localStorage.getItem('infoToken') == null) {
            this.router.navigate(['/home']);
            return false;
        }
        else {
            var httpOptions = {
                headers: new http_1.HttpHeaders({ 'Authorization': localStorage.getItem('infoToken') })
            };
            return this.http.get('/api/authenticationToken', httpOptions).map(function (data) {
                if (data[0].success == true) {
                    return true;
                }
            }).catch(function (error) {
                if (error.status === 401) {
                    _this.router.navigate(['/home']);
                    return Observable_1.Observable.of(false);
                }
            });
        }
    };
    DashAuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router, http_1.HttpClient])
    ], DashAuthGuard);
    return DashAuthGuard;
}());
exports.DashAuthGuard = DashAuthGuard;


/***/ }),

/***/ "../../../../../src/app/shared/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/app/shared/modules/index.ts"));
__export(__webpack_require__("../../../../../src/app/shared/pipes/shared-pipes.module.ts"));
__export(__webpack_require__("../../../../../src/app/shared/modules/index.ts"));
__export(__webpack_require__("../../../../../src/app/shared/guard/index.ts"));


/***/ }),

/***/ "../../../../../src/app/shared/modules/index.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__("../../../../../src/app/shared/modules/page-header/page-header.module.ts"));
__export(__webpack_require__("../../../../../src/app/shared/modules/stat/stat.module.ts"));


/***/ }),

/***/ "../../../../../src/app/shared/modules/page-header/page-header.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n    <div class=\"col-xl-12\">\n        <h2 class=\"page-header\">\n            {{heading}}\n        </h2>\n        <ol class=\"breadcrumb\">\n            <li class=\"breadcrumb-item\">\n                <i class=\"fa fa-dashboard\"></i> <a href=\"Javascript:void(0)\" [routerLink]=\"['/dashboard']\">Dashboard</a>\n            </li>\n            <li class=\"breadcrumb-item active\"><i class=\"fa {{icon}}\"></i> {{heading}}</li>\n        </ol>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/modules/page-header/page-header.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/modules/page-header/page-header.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var PageHeaderComponent = (function () {
    function PageHeaderComponent() {
    }
    PageHeaderComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PageHeaderComponent.prototype, "heading", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], PageHeaderComponent.prototype, "icon", void 0);
    PageHeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-page-header',
            template: __webpack_require__("../../../../../src/app/shared/modules/page-header/page-header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/modules/page-header/page-header.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PageHeaderComponent);
    return PageHeaderComponent;
}());
exports.PageHeaderComponent = PageHeaderComponent;


/***/ }),

/***/ "../../../../../src/app/shared/modules/page-header/page-header.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var page_header_component_1 = __webpack_require__("../../../../../src/app/shared/modules/page-header/page-header.component.ts");
var PageHeaderModule = (function () {
    function PageHeaderModule() {
    }
    PageHeaderModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, router_1.RouterModule],
            declarations: [page_header_component_1.PageHeaderComponent],
            exports: [page_header_component_1.PageHeaderComponent]
        })
    ], PageHeaderModule);
    return PageHeaderModule;
}());
exports.PageHeaderModule = PageHeaderModule;


/***/ }),

/***/ "../../../../../src/app/shared/modules/stat/stat.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card text-white bg-{{bgClass}}\">\n    <div class=\"card-header\">\n        <div class=\"row\">\n            <div class=\"col col-xs-3\">\n                <i class=\"fa {{icon}} fa-5x\"></i>\n            </div>\n            <div class=\"col col-xs-9 text-right\">\n                <div class=\"d-block huge\">{{count}}</div>\n                <div class=\"d-block\">{{label}}</div>\n            </div>\n        </div>\n    </div>\n    <div class=\"card-footer\">\n        <span class=\"float-left\">View Details {{data}}</span>\n        <a href=\"javascript:void(0)\" class=\"float-right card-inverse\">\n            <span ><i class=\"fa fa-arrow-circle-right\"></i></span>\n        </a>\n    </div>\n</div>\n"

/***/ }),

/***/ "../../../../../src/app/shared/modules/stat/stat.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/shared/modules/stat/stat.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var StatComponent = (function () {
    function StatComponent() {
        this.event = new core_1.EventEmitter();
    }
    StatComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "bgClass", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "icon", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "count", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], StatComponent.prototype, "label", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Number)
    ], StatComponent.prototype, "data", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], StatComponent.prototype, "event", void 0);
    StatComponent = __decorate([
        core_1.Component({
            selector: 'app-stat',
            template: __webpack_require__("../../../../../src/app/shared/modules/stat/stat.component.html"),
            styles: [__webpack_require__("../../../../../src/app/shared/modules/stat/stat.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], StatComponent);
    return StatComponent;
}());
exports.StatComponent = StatComponent;


/***/ }),

/***/ "../../../../../src/app/shared/modules/stat/stat.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var stat_component_1 = __webpack_require__("../../../../../src/app/shared/modules/stat/stat.component.ts");
var StatModule = (function () {
    function StatModule() {
    }
    StatModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [stat_component_1.StatComponent],
            exports: [stat_component_1.StatComponent]
        })
    ], StatModule);
    return StatModule;
}());
exports.StatModule = StatModule;


/***/ }),

/***/ "../../../../../src/app/shared/pipes/shared-pipes.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var SharedPipesModule = (function () {
    function SharedPipesModule() {
    }
    SharedPipesModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule
            ],
            declarations: []
        })
    ], SharedPipesModule);
    return SharedPipesModule;
}());
exports.SharedPipesModule = SharedPipesModule;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic()
    .bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map