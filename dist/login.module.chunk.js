webpackJsonp(["login.module"],{

/***/ "../../../../../src/app/patient-login/login-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var login_component_1 = __webpack_require__("../../../../../src/app/patient-login/login.component.ts");
var routes = [
    {
        path: '',
        component: login_component_1.LoginComponent
    }
];
var LoginRoutingModule = (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());
exports.LoginRoutingModule = LoginRoutingModule;


/***/ }),

/***/ "../../../../../src/app/patient-login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<body style=\"background-image:url('https://static1.squarespace.com/static/541ca6ace4b09194f7659e16/t/541fb564e4b0a6db31384587/1411364199494/bridge-lake-bokeh-hd-wallpaper.jpg') no-repeat center;background-size: cover; background-attachment: fixed;height:100%\">\n    <ngx-loading [show]=\"loading\"></ngx-loading>\n    <div class=\"container\">\n        <h1 class=\"title-agile text-center\"></h1>\n        <div class=\"content-w3ls\">\n            <div class=\"content-top-agile\">\n                <h2>sign in</h2>\n            </div>\n            <div class=\"content-bottom\">\n                <form [formGroup]=\"loginForm\">\n                    <div class=\"form-content\">\n                        <div class=\"form-group\">\n                            <mat-form-field>\n                                <input matInput color=\"warn\" placeholder=\"Enter your email\" formControlName=\"email\" required>\n                                <mat-icon matSuffix color=\"warn\">email</mat-icon>\n                                <mat-error *ngIf=\"loginForm.get('email').hasError('required')\">This field has to be filled</mat-error>\n                                <mat-error *ngIf=\"loginForm.get('email').hasError('email')\">Enter valid email</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <div class=\"form-content\">\n                        <div class=\"form-group\">\n                            <mat-form-field>\n                                <input matInput color=\"warn\" placeholder=\"Enter your password\" [type]=\"hide ? 'password' : 'text'\" formControlName=\"password\"\n                                    required>\n                                <mat-icon matSuffix color=\"warn\" (click)=\"hide = !hide\">{{hide ? 'visibility' : 'visibility_off'}}</mat-icon>\n                                <mat-error *ngIf=\"loginForm.get('password').hasError('required')\">This field has to be filled</mat-error>\n                            </mat-form-field>\n                        </div>\n                    </div>\n                    <ul class=\"list-login\">\n                        <li class=\"switch-agileits\">\n                            <label class=\"switch\">\n                                <input type=\"checkbox\">\n                                <span class=\"slider round\"></span>\n                                keep me signed in\n                            </label>\n                        </li>\n                        <li>\n                            <a [routerLink]=\"['/signup/patient']\" class=\"text-right\">sign up</a>\n                        </li>\n                        <li class=\"clearfix\"></li>\n                    </ul>\n                    <div class=\"wthree-field\">\n                        <button id=\"saveForm\" type=\"submit\" (click)=\"onLoggedin()\" [disabled]=\"!loginForm.valid\">Login</button>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/patient-login/login.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\n  background: url(\"https://static1.squarespace.com/static/541ca6ace4b09194f7659e16/t/541fb564e4b0a6db31384587/1411364199494/bridge-lake-bokeh-hd-wallpaper.jpg\") no-repeat center;\n  background-size: cover;\n  -webkit-background-size: cover;\n  -moz-background-size: cover;\n  -o-background-size: cover;\n  -ms-background-size: cover;\n  background-attachment: fixed;\n  font-family: 'Mukta Mahee', sans-serif; }\n\nsection {\n  display: block; }\n\nol,\nul {\n  list-style: none;\n  margin: 0px;\n  padding: 0px; }\n\n/*--start editing from here--*/\n\na {\n  text-decoration: none; }\n\n.txt-rt {\n  text-align: right; }\n\n/* text align right */\n\n.txt-lt {\n  text-align: left; }\n\n/* text align left */\n\n.txt-center {\n  text-align: center; }\n\n/* text align center */\n\n.float-rt {\n  float: right; }\n\n/* float right */\n\n.float-lt {\n  float: left; }\n\n/* float left */\n\n.clearfix {\n  clear: both; }\n\n/* clear float */\n\n.pos-relative {\n  position: relative; }\n\n/* Position Relative */\n\n.pos-absolute {\n  position: absolute; }\n\n.content-w3ls {\n  max-width: 450px;\n  margin: 3em auto; }\n\n.content-bottom {\n  padding: 3em;\n  background: #fff;\n  border-radius: 0 0 25px 25px; }\n\n.content-top-agile {\n  background: rgba(0, 0, 0, 0.28);\n  padding: 3em;\n  text-align: center;\n  border-radius: 25px 25px 0 0;\n  text-transform: uppercase;\n  position: relative; }\n\n.content-top-agile:after {\n  content: '';\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  width: 100%;\n  display: block;\n  background: -webkit-gradient(linear, left top, right top, from(rgba(0, 128, 0, 0.7)), color-stop(rgba(0, 0, 255, 0.6)), color-stop(rgba(75, 0, 130, 0.6)), color-stop(rgba(238, 130, 238, 0.6)), color-stop(rgba(255, 0, 0, 0.6)), color-stop(rgba(255, 165, 0, 0.6)), to(rgba(255, 255, 0, 0.6)));\n  background: linear-gradient(to right, rgba(0, 128, 0, 0.7), rgba(0, 0, 255, 0.6), rgba(75, 0, 130, 0.6), rgba(238, 130, 238, 0.6), rgba(255, 0, 0, 0.6), rgba(255, 165, 0, 0.6), rgba(255, 255, 0, 0.6));\n  background: -webkit-gradient(to right, rgba(0, 128, 0, 0.7), rgba(0, 0, 255, 0.6), rgba(75, 0, 130, 0.6), rgba(238, 130, 238, 0.6), rgba(255, 0, 0, 0.6), rgba(255, 165, 0, 0.6), rgba(255, 255, 0, 0.6));\n  background: -ms-linear-gradient(to right, rgba(0, 128, 0, 0.7), rgba(0, 0, 255, 0.6), rgba(75, 0, 130, 0.6), rgba(238, 130, 238, 0.6), rgba(255, 0, 0, 0.6), rgba(255, 165, 0, 0.6), rgba(255, 255, 0, 0.6)); }\n\n.content-top-agile h2 {\n  color: #ffffff;\n  font-weight: 600;\n  font-size: 2em;\n  letter-spacing: 4px;\n  word-spacing: 2px; }\n\nh1.title-agile {\n  padding: 1em 0 0;\n  text-transform: uppercase;\n  color: #fff;\n  font-size: 2.5em;\n  text-shadow: 3px 3px 1px #000;\n  letter-spacing: 4px; }\n\n.content-w3ls ul li {\n  display: inline-block; }\n\nli.switch-agileits {\n  float: left; }\n\nul.list-login li:nth-child(2) {\n  float: right; }\n\nli:nth-child(2) a {\n  color: #000; }\n\n/* form style  */\n\nform .field-group {\n  background: #da0845;\n  display: flex;\n  display: -webkit-box;\n  /* OLD - iOS 6-, Safari 3.1-6 */\n  /* OLD - Firefox 19- (buggy but mostly works) */\n  display: -ms-flexbox;\n  /* TWEENER - IE 10 */\n  display: -webkit-flex;\n  /* NEW - Chrome */\n  margin: 0 0 12px 0; }\n\nform .field-group span {\n  -webkit-box-flex: 1;\n  /* OLD - iOS 6-, Safari 3.1-6 */\n  -moz-box-flex: 1;\n  /* OLD - Firefox 19- */\n  width: 20%;\n  /* For old syntax, otherwise collapses. */\n  /* Chrome */\n  -ms-flex: 1;\n  /* IE 10 */\n  flex: 1;\n  /* NEW, Spec - Opera 12.1, Firefox 20+ */\n  color: #ffffff;\n  font-size: 1.4em;\n  text-align: center;\n  line-height: 48px; }\n\nul.list-login {\n  margin: 2em 0; }\n\nform .field-group .wthree-field {\n  flex: 3 50%;\n  -webkit-box-flex: 3 50%;\n  /* OLD - iOS 6-, Safari 3.1-6 */\n  -moz-box-flex: 3 50%;\n  /* OLD - Firefox 19- */\n  -webkit-flex: 3 50%;\n  /* Chrome */\n  -ms-flex: 3 50%;\n  /* IE 10 */ }\n\n.wthree-field input {\n  padding: 13px 15px;\n  font-size: 16px;\n  border: none;\n  border-bottom: 1px solid #da0845;\n  background: #fff;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  width: 100%;\n  font-family: 'Mukta Mahee', sans-serif;\n  outline: none; }\n\n.wthree-field button[type=\"submit\"] {\n  background: #da0845;\n  width: 100%;\n  border: none;\n  color: #fff;\n  padding: 12px 15px;\n  text-transform: uppercase;\n  font-size: 16px;\n  font-weight: 600;\n  font-family: 'Mukta Mahee', sans-serif;\n  cursor: pointer;\n  transition: 0.5s all;\n  -webkit-transition: 0.5s all;\n  -moz-transition: 0.5s all;\n  -o-transition: 0.5s all;\n  -ms-transition: 0.5s all; }\n\n.wthree-field button[type=\"submit\"]:hover {\n  background: #000;\n  color: #da0845;\n  letter-spacing: 2px; }\n\n/* switch */\n\nlabel.switch {\n  position: relative;\n  display: inline-block;\n  height: 23px;\n  padding-left: 5em;\n  cursor: pointer;\n  color: #000; }\n\nli:nth-child(2) a,\nlabel.switch {\n  text-transform: uppercase;\n  font-size: 13px;\n  font-weight: 600;\n  letter-spacing: 1px; }\n\n.switch input {\n  display: none; }\n\n.slider {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  width: 26%;\n  background-color: #777;\n  -webkit-transition: .4s;\n  transition: .4s; }\n\n.slider:before {\n  position: absolute;\n  content: \"\";\n  height: 15px;\n  width: 15px;\n  left: 4px;\n  bottom: 4px;\n  background-color: white;\n  -webkit-transition: .4s;\n  transition: .4s; }\n\ninput:checked + .slider {\n  background-color: #da0845; }\n\ninput:focus + .slider {\n  -webkit-box-shadow: 0 0 1px #2196F3;\n          box-shadow: 0 0 1px #2196F3; }\n\ninput:checked + .slider:before {\n  -webkit-transform: translateX(26px);\n  transform: translateX(26px); }\n\n/* Rounded sliders */\n\n.slider.round {\n  border-radius: 34px; }\n\n.slider.round:before {\n  border-radius: 50%; }\n\n/* //switch */\n\n.copyright p {\n  color: #fff;\n  letter-spacing: 1px;\n  padding: 0 1em 2em; }\n\n.copyright p a {\n  color: #fff; }\n\n/* //content */\n\n@media screen and (max-width: 768px) {\n  .content-top-agile {\n    padding: 2.5em; } }\n\n@media screen and (max-width: 480px) {\n  .content-w3ls {\n    margin: 3em 1em; }\n  h1.title-agile {\n    font-size: 2.3em;\n    letter-spacing: 3px; }\n  label.switch {\n    padding-left: 4em; }\n  .content-top-agile h2 {\n    font-size: 1.8em; } }\n\n@media screen and (max-width: 414px) {\n  h1.title-agile {\n    font-size: 2.2em; }\n  ul.list-login li:nth-child(2),\n  li.switch-agileits {\n    float: none; }\n  ul.list-login li:nth-child(2) {\n    margin-top: 1em; }\n  form .field-group .wthree-field {\n    -webkit-box-flex: 3;\n        -ms-flex: 3 40%;\n            flex: 3 40%; } }\n\n@media screen and (max-width: 384px) {\n  h1.title-agile {\n    font-size: 2.1em; }\n  .content-w3ls {\n    margin: 2em 1em; }\n  .wthree-field input[type=\"submit\"] {\n    padding: 7px 15px; } }\n\n@media screen and (max-width: 375px) {\n  h1.title-agile {\n    font-size: 2em; }\n  .content-top-agile {\n    padding: 2em; }\n  .content-bottom {\n    padding: 2em; }\n  .copyright p {\n    letter-spacing: 0.5px; } }\n\n@media screen and (max-width: 320px) {\n  h1.title-agile {\n    font-size: 1.7em; }\n  .content-bottom {\n    padding: 2em 1.5em; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/patient-login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var wow_min_1 = __webpack_require__("../../../../wowjs/dist/wow.min.js");
var router_animations_1 = __webpack_require__("../../../../../src/app/router.animations.ts");
var login_service_1 = __webpack_require__("../../../../../src/app/patient-login/login.service.ts");
var LoginComponent = (function () {
    function LoginComponent(fb, loginService) {
        this.fb = fb;
        this.loginService = loginService;
        this.hide = true;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loginForm = this.fb.group({
            email: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.email]),
            password: new forms_1.FormControl('', [forms_1.Validators.required])
        });
        this.loginService.loading_subject.subscribe(function (load) {
            _this.loading = load;
        });
    };
    LoginComponent.prototype.ngAfterViewInit = function () {
        new wow_min_1.WOW().init();
    };
    LoginComponent.prototype.onLoggedin = function () {
        this.loginService.LoginWithEmailAndPassword(this.loginForm.value);
        this.loginForm.reset();
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/patient-login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/patient-login/login.component.scss")],
            animations: [router_animations_1.routerTransition()]
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder, login_service_1.PatientLoginService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "../../../../../src/app/patient-login/login.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var ngx_loading_1 = __webpack_require__("../../../../ngx-loading/ngx-loading/ngx-loading.es5.js");
var login_routing_module_1 = __webpack_require__("../../../../../src/app/patient-login/login-routing.module.ts");
var login_service_1 = __webpack_require__("../../../../../src/app/patient-login/login.service.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/patient-login/login.component.ts");
var LoginModule = (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, material_1.MatInputModule,
                material_1.MatSelectModule,
                material_1.MatFormFieldModule,
                material_1.MatIconModule,
                forms_1.ReactiveFormsModule,
                material_1.MatSnackBarModule,
                login_routing_module_1.LoginRoutingModule, forms_1.FormsModule, ngx_loading_1.LoadingModule.forRoot({
                    animationType: ngx_loading_1.ANIMATION_TYPES.rectangleBounce,
                    backdropBackgroundColour: 'rgba(0,0,0,0.5)',
                    backdropBorderRadius: '4px',
                    primaryColour: '#ffffff',
                    secondaryColour: '#ffffff',
                    tertiaryColour: '#ffffff'
                })],
            declarations: [login_component_1.LoginComponent],
            providers: [login_service_1.PatientLoginService]
        })
    ], LoginModule);
    return LoginModule;
}());
exports.LoginModule = LoginModule;


/***/ }),

/***/ "../../../../../src/app/patient-login/login.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var PatientLoginService = (function () {
    function PatientLoginService(http, router, snackBar) {
        this.http = http;
        this.router = router;
        this.snackBar = snackBar;
        this.loading_subject = new Subject_1.Subject();
    }
    PatientLoginService.prototype.LoginWithEmailAndPassword = function (registerForm) {
        var _this = this;
        this.loading = true;
        this.loading_subject.next(this.loading);
        this.http.post('/api/patient-login', registerForm).subscribe(function (response) {
            if (response) {
                if (response[0].exist == true) {
                    if (response[0].jwtToken) {
                        localStorage.setItem('jwtToken', response[0].jwtToken);
                        localStorage.setItem('firstTime', 'true');
                        _this.router.navigate(['/patient/information']);
                        _this.loading = false;
                        _this.loading_subject.next(_this.loading);
                    }
                    else if (response[0].infoToken) {
                        localStorage.setItem('infoToken', response[0].infoToken);
                        _this.router.navigate(['/dashboard']);
                        _this.loading = false;
                        _this.loading_subject.next(_this.loading);
                    }
                }
                else {
                    _this.loading = false;
                    _this.loading_subject.next(_this.loading);
                    _this.snackBar.open('Wrong Email or Password', '', {
                        duration: 4000
                    });
                }
            }
        }, function (error) {
            console.log("error");
        });
    };
    PatientLoginService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router, material_1.MatSnackBar])
    ], PatientLoginService);
    return PatientLoginService;
}());
exports.PatientLoginService = PatientLoginService;


/***/ })

});
//# sourceMappingURL=login.module.chunk.js.map