webpackJsonp(["layout.module"],{

/***/ "../../../../../src/app/layout/components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark fixed-top\">\n    <div class=\"container\">\n        <a class=\"navbar-brand mr-auto\" href=\"#\">Denatal Project</a>\n        <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarResponsive\"\n            aria-controls=\"navbarResponsive\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n            Menu\n            <i class=\"fa fa-bars\"></i>\n        </button>\n        <div class=\"collapse navbar-collapse justify-content-between\" id=\"navbarResponsive\">\n            <ul class=\"navbar-nav ml-auto\">\n                <li class=\"nav-item\">\n                    <a routerLink=\"/patient/blog\" [routerLinkActive]=\"['router-link-active']\" class=\"nav-link js-scroll-trigger\">\n                        <i class=\"socicon-blogger\"></i> &nbsp;Blog\n                    </a>\n                </li>\n                <span class=\"divide d-none d-md-block\" style=\"color:#fff;margin-top:7px\">|</span>\n                <li class=\"nav-item dropdown\" ngbDropdown>\n                    <a href=\"javascript:void(0)\" class=\"nav-link js-scroll-trigger\" ngbDropdownToggle>\n                        <i class=\"fa fa-user\"></i> &nbsp; {{name | uppercase}}\n                        <b class=\"caret\"></b>\n                    </a>\n                    <div class=\"dropdown-menu-left animated zoomInDown\" ngbDropdownMenu style=\"float:left;\">\n                        <a class=\"dropdown-item\" href=\"javascript:void(0)\">\n                            <i class=\"fa fa-fw fa-user\"></i> {{ 'Profile' | translate }}\n                        </a>\n                        <a href=\"javascript:void(0)\" class=\"dropdown-item\">\n                            <i class=\"fa fa-cogs\"></i> &nbsp;setting\n                        </a>\n                        <a class=\"dropdown-item\" [routerLink]=\"['/login']\" (click)=\"onLoggedout()\">\n                            <i class=\"fa fa-fw fa-power-off\"></i> {{ 'Log Out' | translate }}\n                        </a>\n                    </div>\n                </li>\n            </ul>\n        </div>\n    </div>\n</nav>"

/***/ }),

/***/ "../../../../../src/app/layout/components/header/header.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ":host .navbar {\n  background-color: #000000;\n  -webkit-transition: all 0.6s ease-out;\n  transition: all 0.6s ease-out; }\n  :host .navbar .navbar-brand {\n    color: #fff; }\n  :host .navbar .nav-item > a {\n    color: #fff; }\n  :host .navbar .nav-item > a:hover {\n      color: yellow;\n      border-bottom: yellow 2px solid;\n      -webkit-transition: all 0.5s ease-in-out;\n      transition: all 0.5s ease-in-out; }\n  :host .router-link-active {\n  border-bottom: yellow 2px solid; }\n  @media (max-width: 990px) {\n  :host .navbar {\n    background-color: #000000;\n    position: relative; } }\n  :host .scrolled {\n  background: rgba(0, 0, 0, 0.78); }\n  :host .messages {\n  width: 300px; }\n  :host .messages .media {\n    border-bottom: 1px solid #ddd;\n    padding: 5px 10px; }\n  :host .messages .media:last-child {\n      border-bottom: none; }\n  :host .messages .media-body h5 {\n    font-size: 13px;\n    font-weight: 600; }\n  :host .messages .media-body .small {\n    margin: 0; }\n  :host .messages .media-body .last {\n    font-size: 12px;\n    margin: 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layout/components/header/header.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var core_2 = __webpack_require__("../../../../@ngx-translate/core/index.js");
var header_service_1 = __webpack_require__("../../../../../src/app/layout/components/header/header.service.ts");
var $ = __webpack_require__("../../../../jquery/dist/jquery.js");
var HeaderComponent = (function () {
    function HeaderComponent(translate, router, headerService) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.headerService = headerService;
        this.pushRightClass = 'push-right';
        this.toggle = false;
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof router_1.NavigationEnd &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    HeaderComponent.prototype.ngOnInit = function () {
        if ($('.navbar').length > 0) {
            $(window).on("scroll load resize", function () {
                var startY = $('.navbar').height() * 2;
                if ($(window).scrollTop() > startY) {
                    $('.navbar').addClass("scrolled");
                }
                else {
                    $('.navbar').removeClass("scrolled");
                }
            });
        }
    };
    HeaderComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    HeaderComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    HeaderComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    HeaderComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('infoToken');
        this.router.navigate(['/home']);
    };
    HeaderComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], HeaderComponent.prototype, "name", void 0);
    HeaderComponent = __decorate([
        core_1.Component({
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/layout/components/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layout/components/header/header.component.scss")]
        }),
        __metadata("design:paramtypes", [core_2.TranslateService, router_1.Router, header_service_1.HeaderService])
    ], HeaderComponent);
    return HeaderComponent;
}());
exports.HeaderComponent = HeaderComponent;


/***/ }),

/***/ "../../../../../src/app/layout/components/header/header.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var decode = __webpack_require__("../../../../jwt-decode/lib/index.js");
var HeaderService = (function () {
    function HeaderService(http, router) {
        this.http = http;
        this.router = router;
        this.loading_subject = new Subject_1.Subject();
    }
    HeaderService.prototype.ngOnInit = function () {
        var token = localStorage.getItem('infoToken');
        var tokenPayload = decode(token);
        console.log(tokenPayload);
    };
    HeaderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, router_1.Router])
    ], HeaderService);
    return HeaderService;
}());
exports.HeaderService = HeaderService;


/***/ }),

/***/ "../../../../../src/app/layout/components/sidebar/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"bars\" (click)=\"togglebar()\">\n        <div class=\"bar1\"></div>\n        <div class=\"bar2\"></div>\n        <div class=\"bar3\"></div>\n    </div>\n    <nav class=\"sidebar\" [ngClass]=\"{sidebarPushRight: isActive}\">\n        <div class=\"list-group removeget\">\n            <a routerLink=\"/dashboard\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                <i class=\"fa fa-fw fa-home\"></i>&nbsp;{{ 'Home' | translate }}\n            </a>\n            <a [routerLink]=\"['/patient/appointment']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                <i class=\"fa fa-fw fa-address-book\"></i>&nbsp;{{ 'Appointment' | translate }}\n            </a>\n            <a [routerLink]=\"['/patient/query']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                <i class=\"fa fa-fw fa-exclamation-triangle\"></i>&nbsp;{{ 'Query' | translate }}\n            </a>\n            <a [routerLink]=\"['/dashboard']\" [routerLinkActive]=\"['router-link-active']\" class=\"list-group-item\">\n                <i class=\"fa fa-fw fa-sticky-note\"></i>&nbsp;{{ 'Prescription' | translate }}\n            </a>\n            <a class=\"list-group-item\" [routerLinkActive]=\"['router-link-active']\" (click)=\"addExpandClass()\">\n                <span>\n                    <i class=\"fa fa-caret-down\"></i>&nbsp; {{ 'More' | translate }}</span>\n            </a>\n            <li class=\"nested\" [class.expand]=\"showMenu\">\n                <ul class=\"submenu animated slideInDown\">\n                    <li>\n                        <a href=\"javascript:void(0)\">\n                            <i class=\"fa fa-fw fa-history\"></i>&nbsp; {{ 'History' | translate }}</a>\n                    </li>\n                    <li>\n                        <a href=\"javascript:void(0)\">\n                            <i class=\"fa fa-fw fa-table\"></i>&nbsp; {{ 'Statistics' | translate }}</a>\n                    </li>\n                </ul>\n            </li>\n        </div>\n    </nav>\n</div>"

/***/ }),

/***/ "../../../../../src/app/layout/components/sidebar/sidebar.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@media screen and (max-width: 990px) {\n  .bars {\n    display: block;\n    cursor: pointer;\n    padding-top: 20px;\n    position: fixed;\n    z-index: 3000; }\n  .bar1, .bar2, .bar3 {\n    width: 25px;\n    height: 5px;\n    background-color: #333;\n    margin: 6px 0;\n    -webkit-transition: 0.4s;\n    transition: 0.4s; }\n  .change .bar1 {\n    -webkit-transform: rotate(-45deg) translate(-9px, 6px);\n    transform: rotate(-45deg) translate(-9px, 6px); }\n  .change .bar2 {\n    opacity: 0; }\n  .change .bar3 {\n    -webkit-transform: rotate(45deg) translate(-8px, -8px);\n    transform: rotate(45deg) translate(-8px, -8px); } }\n\n.sidebar {\n  border-radius: 0;\n  position: fixed;\n  z-index: 1000;\n  top: 0px;\n  left: 235px;\n  width: 200px;\n  margin-left: -235px;\n  border: none;\n  border-radius: 0;\n  overflow-y: auto;\n  background-color: rgba(0, 0, 0, 0.7);\n  bottom: 0;\n  overflow-x: hidden;\n  padding-bottom: 40px;\n  -webkit-transition: all 0.2s ease-in-out;\n  transition: all 0.2s ease-in-out; }\n\n.sidebar .list-group a.list-group-item {\n    top: 56px;\n    background: rgba(0, 0, 0, 0.7);\n    border: 0;\n    border-radius: 0;\n    color: #fff;\n    text-decoration: none;\n    -webkit-transition: all 0.5s ease-in-out;\n    transition: all 0.5s ease-in-out; }\n\n.sidebar .list-group a.list-group-item .fa {\n      margin-right: 10px; }\n\n.sidebar .list-group a.list-group-item:hover {\n    background: rgba(13, 13, 13, 0.7);\n    color: #fff;\n    font-size: 1.1em;\n    -webkit-transition: all 0.5s ease-in-out;\n    transition: all 0.5s ease-in-out; }\n\n.sidebar .list-group a.list-group-item:hover:nth-child(1) {\n    border-left: 5px solid #2ECC40; }\n\n.sidebar .list-group a.list-group-item:hover:nth-child(2) {\n    border-left: 5px solid #F012BE; }\n\n.sidebar .list-group a.list-group-item:hover:nth-child(3) {\n    border-left: 5px solid #FF4136; }\n\n.sidebar .list-group a.list-group-item:hover:nth-child(4) {\n    border-left: 5px solid #FFDC00; }\n\n.sidebar .list-group a.list-group-item:hover:nth-child(5) {\n    border-left: 5px solid #0074D9; }\n\n.sidebar .list-group a.router-link-active:nth-child(1) {\n    background: rgba(0, 0, 0, 0.7);\n    color: #fff;\n    border-left: 5px solid #2ECC40; }\n\n.sidebar .list-group a.router-link-active:nth-child(2) {\n    background: rgba(0, 0, 0, 0.7);\n    color: #fff;\n    border-left: 5px solid #F012BE; }\n\n.sidebar .list-group a.router-link-active:nth-child(3) {\n    background: rgba(0, 0, 0, 0.7);\n    color: #fff;\n    border-left: 5px solid #FF4136; }\n\n.sidebar .list-group a.router-link-active:nth-child(4) {\n    background: rgba(0, 0, 0, 0.7);\n    color: #fff;\n    border-left: 5px solid #FFDC00; }\n\n.sidebar .list-group a.router-link-active:nth-child(5) {\n    background: rgba(0, 0, 0, 0.7);\n    color: #fff;\n    border-left: 5px solid #0074D9; }\n\n.sidebar .list-group .header-fields {\n    padding-top: 10px; }\n\n.sidebar .list-group .header-fields > .list-group-item:first-child {\n      border-top: 1px solid rgba(255, 255, 255, 0.2); }\n\n.sidebar .sidebar-dropdown *:focus {\n    border-radius: none;\n    border: none; }\n\n.sidebar .sidebar-dropdown .panel-title {\n    font-size: 1rem;\n    height: 50px;\n    margin-bottom: 0; }\n\n.sidebar .sidebar-dropdown .panel-title a {\n      color: #999;\n      text-decoration: none;\n      font-weight: 400;\n      background: rgba(0, 0, 0, 0.7); }\n\n.sidebar .sidebar-dropdown .panel-title a span {\n        position: relative;\n        display: block;\n        padding: 0.75rem 1.5rem;\n        padding-top: 1rem; }\n\n.sidebar .sidebar-dropdown .panel-title a:hover,\n    .sidebar .sidebar-dropdown .panel-title a:focus {\n      color: #fff;\n      outline: none;\n      outline-offset: -2px; }\n\n.sidebar .sidebar-dropdown .panel-title:hover {\n    background: rgba(0, 0, 0, 0.7); }\n\n.sidebar .sidebar-dropdown .panel-collapse {\n    border-radius: 0;\n    border: none; }\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item {\n      border-radius: 0;\n      background-color: rgba(0, 0, 0, 0.7);\n      border: 0 solid transparent; }\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a {\n        color: #999; }\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item a:hover {\n        color: #fff; }\n\n.sidebar .sidebar-dropdown .panel-collapse .panel-body .list-group-item:hover {\n      background: #2E2E2E; }\n\na.list-group-item {\n  cursor: pointer;\n  color: #fff; }\n\n.nested {\n  list-style-type: none; }\n\nul.submenu {\n  display: none;\n  height: 0; }\n\n.expand ul.submenu {\n  margin-top: 56px;\n  display: block;\n  list-style-type: none;\n  height: auto; }\n\n.expand ul.submenu li a {\n    color: #fff;\n    -webkit-text-decoration-line: none;\n            text-decoration-line: none;\n    padding: 10px;\n    display: block;\n    list-style-type: none; }\n\n.expand ul.submenu li a:hover {\n    background: rgba(13, 13, 13, 0.7);\n    font-size: 1.1em;\n    border-bottom: 5px solid #fff;\n    -webkit-transition: all 0.5s ease-in-out;\n    transition: all 0.5s ease-in-out; }\n\n@media (max-width: 990px) {\n  .sidebar {\n    display: none; } }\n\n@media (min-width: 992px) {\n  .header-fields {\n    display: none; } }\n\n::-webkit-scrollbar {\n  width: 8px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layout/components/sidebar/sidebar.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var core_2 = __webpack_require__("../../../core/esm5/core.js");
var core_3 = __webpack_require__("../../../../@ngx-translate/core/index.js");
var $ = __webpack_require__("../../../../jquery/dist/jquery.js");
var SidebarComponent = (function () {
    function SidebarComponent(translate, router) {
        var _this = this;
        this.translate = translate;
        this.router = router;
        this.isActive = false;
        this.showMenu = false;
        this.showMenu2 = false;
        this.showMenu3 = false;
        this.pushRightClass = 'push-right';
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de/) ? browserLang : 'en');
        this.router.events.subscribe(function (val) {
            if (val instanceof router_1.NavigationEnd &&
                window.innerWidth <= 992 &&
                _this.isToggled()) {
                _this.toggleSidebar();
            }
        });
    }
    SidebarComponent.prototype.onResize = function (event) {
        if (event.target.innerWidth >= 990) {
            $('.bars').addClass('change');
            $('.sidebar').css('display', 'block');
            $('.bars').css('margin-left', '210px');
        }
        else {
            $('.bars').removeClass('change');
            $('.sidebar').css('display', 'none');
            $('.bars').css('margin-left', '0px');
        }
    };
    SidebarComponent.prototype.ngOnInit = function () {
    };
    SidebarComponent.prototype.togglebar = function () {
        if (this.showMenu3 == false) {
            $('.bars').addClass('change');
            $('.bars').css('margin-left', '210px');
            $('.sidebar').css('display', 'block');
        }
        else {
            $('.bars').removeClass('change');
            $('.bars').css('margin-left', '0px');
            $('.sidebar').css('display', 'none');
        }
        this.showMenu3 = !this.showMenu3;
    };
    SidebarComponent.prototype.eventCalled = function () {
        this.isActive = !this.isActive;
    };
    SidebarComponent.prototype.addExpandClass = function () {
        this.showMenu = !this.showMenu;
    };
    SidebarComponent.prototype.MenuExpandClass = function () {
        this.showMenu2 = !this.showMenu2;
    };
    SidebarComponent.prototype.isToggled = function () {
        var dom = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    };
    SidebarComponent.prototype.toggleSidebar = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    };
    SidebarComponent.prototype.rltAndLtr = function () {
        var dom = document.querySelector('body');
        dom.classList.toggle('rtl');
    };
    SidebarComponent.prototype.changeLang = function (language) {
        this.translate.use(language);
    };
    SidebarComponent.prototype.onLoggedout = function () {
        localStorage.removeItem('infoToken');
        this.router.navigate(['/home']);
    };
    __decorate([
        core_2.HostListener('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], SidebarComponent.prototype, "onResize", null);
    __decorate([
        core_1.Input(),
        __metadata("design:type", String)
    ], SidebarComponent.prototype, "name", void 0);
    SidebarComponent = __decorate([
        core_1.Component({
            selector: 'app-sidebar',
            template: __webpack_require__("../../../../../src/app/layout/components/sidebar/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layout/components/sidebar/sidebar.component.scss")]
        }),
        __metadata("design:paramtypes", [core_3.TranslateService, router_1.Router])
    ], SidebarComponent);
    return SidebarComponent;
}());
exports.SidebarComponent = SidebarComponent;


/***/ }),

/***/ "../../../../../src/app/layout/layout-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var layout_component_1 = __webpack_require__("../../../../../src/app/layout/layout.component.ts");
var routes = [
    {
        path: '',
        component: layout_component_1.LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'patient/blog', loadChildren: './patient-blog/blog.module#BlogModule' },
            { path: 'patient/query', loadChildren: './query/query.module#QueryModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'patient/appointment', loadChildren: './appointment/appointment-page.module#AppointmentPageModule' }
        ]
    }
];
var LayoutRoutingModule = (function () {
    function LayoutRoutingModule() {
    }
    LayoutRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], LayoutRoutingModule);
    return LayoutRoutingModule;
}());
exports.LayoutRoutingModule = LayoutRoutingModule;


/***/ }),

/***/ "../../../../../src/app/layout/layout.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header [name]=\"name\" ></app-header>\n<app-sidebar [name]=\"name\" ></app-sidebar>\n<div class=\"main-container\">\n    <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "../../../../../src/app/layout/layout.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".main-container {\n  background-color: #fff;\n  margin-top: 40px;\n  margin-left: 200px;\n  -ms-overflow-x: hidden;\n  overflow-x: hidden;\n  overflow-y: scroll;\n  overflow: hidden; }\n\n@media screen and (max-width: 992px) {\n  .main-container {\n    margin-left: 0px !important; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layout/layout.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var decode = __webpack_require__("../../../../jwt-decode/lib/index.js");
var LayoutComponent = (function () {
    function LayoutComponent() {
    }
    LayoutComponent.prototype.ngOnInit = function () {
        localStorage.removeItem('jwtToken');
        var token = localStorage.getItem('infoToken');
        var tokenPayload = decode(token);
        this.name = tokenPayload.firstname;
        this.email = tokenPayload.email;
        this.phone = tokenPayload.mobile_number;
    };
    LayoutComponent = __decorate([
        core_1.Component({
            selector: 'app-layout',
            template: __webpack_require__("../../../../../src/app/layout/layout.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layout/layout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], LayoutComponent);
    return LayoutComponent;
}());
exports.LayoutComponent = LayoutComponent;


/***/ }),

/***/ "../../../../../src/app/layout/layout.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var core_2 = __webpack_require__("../../../../@ngx-translate/core/index.js");
var ng_bootstrap_1 = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
var layout_routing_module_1 = __webpack_require__("../../../../../src/app/layout/layout-routing.module.ts");
var patient_home_module_1 = __webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home.module.ts");
var layout_component_1 = __webpack_require__("../../../../../src/app/layout/layout.component.ts");
var sidebar_component_1 = __webpack_require__("../../../../../src/app/layout/components/sidebar/sidebar.component.ts");
var header_component_1 = __webpack_require__("../../../../../src/app/layout/components/header/header.component.ts");
var header_service_1 = __webpack_require__("../../../../../src/app/layout/components/header/header.service.ts");
var LayoutModule = (function () {
    function LayoutModule() {
    }
    LayoutModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                layout_routing_module_1.LayoutRoutingModule,
                core_2.TranslateModule,
                patient_home_module_1.PatientHomeModule,
                ng_bootstrap_1.NgbDropdownModule.forRoot()
            ],
            declarations: [layout_component_1.LayoutComponent, sidebar_component_1.SidebarComponent, header_component_1.HeaderComponent],
            providers: [header_service_1.HeaderService]
        })
    ], LayoutModule);
    return LayoutModule;
}());
exports.LayoutModule = LayoutModule;


/***/ }),

/***/ "../../../../../src/app/layout/patient-home/patient-home/patient-home-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var patient_home_component_1 = __webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home.component.ts");
var routes = [
    {
        path: '', component: patient_home_component_1.PatientHomeComponent
    }
];
var PatientHomeRoutingModule = (function () {
    function PatientHomeRoutingModule() {
    }
    PatientHomeRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], PatientHomeRoutingModule);
    return PatientHomeRoutingModule;
}());
exports.PatientHomeRoutingModule = PatientHomeRoutingModule;


/***/ }),

/***/ "../../../../../src/app/layout/patient-home/patient-home/patient-home.component.html":
/***/ (function(module, exports) {

module.exports = "<header>\n\t<nav class=\"navbar navbar-default navbar-fixed-top\" data-aos=\"fade-down\" data-aos-duration=\"2600\" data-aos-once=\"true\">\n\t\t<div class=\"container-fluid\">\n\t\t\t<div class=\"navbar-header\">\n\t\t\t\t<a class=\"navbar-brand navbar-link\" href=\"#\">Dize Design AOS-Header v.2</a>\n\t\t\t\t<button class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navcol-1\">\n\t\t\t\t\t<span class=\"sr-only\">Toggle navigation</span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t\t<span class=\"icon-bar\"></span>\n\t\t\t\t</button>\n\t\t\t</div>\n\t\t\t<div class=\"collapse navbar-collapse\" id=\"navcol-1\">\n\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">\n\t\t\t\t\t<li role=\"presentation\">\n\t\t\t\t\t\t<a href=\"#\">Home </a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li role=\"presentation\">\n\t\t\t\t\t\t<a href=\"#\">Project </a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li role=\"presentation\">\n\t\t\t\t\t\t<a href=\"#\">About Us</a>\n\t\t\t\t\t</li>\n\t\t\t\t\t<li role=\"presentation\">\n\t\t\t\t\t\t<a href=\"#\">Contact </a>\n\t\t\t\t\t</li>\n\t\t\t\t</ul>\n\t\t\t</div>\n\t\t</div>\n\t</nav>\n\n\t<div class=\"container\">\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-md-12\" data-aos=\"fade-left\" data-aos-duration=\"2600\" data-aos-once=\"true\">\n\t\t\t\t<h1>Lorem Ipsum</h1>\n\t\t\t\t<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>\n\t\t\t</div>\n\t\t</div>\n\t</div>\n\n</header>"

/***/ }),

/***/ "../../../../../src/app/layout/patient-home/patient-home/patient-home.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "@charset \"UTF-8\";\n/* Demo */\nbody {\n  font-family: 'sans-serif'; }\n/* Header settings */\nheader {\n  height: 100vh;\n  /* you can change that height to 100%, please get sure youre Header is ready */\n  background-attachment: fixed;\n  background-size: cover;\n  background-position: left;\n  background-repeat: no-repeat;\n  background-image: -webkit-gradient(linear, left top, right top, from(black), to(white)), url(\"https://static.pexels.com/photos/225600/pexels-photo-225600.jpeg\");\n  background-image: linear-gradient(to right, black 0%, white 100%), url(\"https://static.pexels.com/photos/225600/pexels-photo-225600.jpeg\");\n  background-blend-mode: screen;\n  /* you can remove that, if you do that delet the \"linear-gradient(to right, black 0%, white 100%),\" */ }\n@media (max-width: 767px) {\n  header {\n    background-attachment: inherit; } }\nheader .row {\n  margin-top: 40vh;\n  /* Handle the row */ }\n@media (max-width: 767px) {\n  header .row {\n    margin-top: 30vh;\n    /* Handle the row on mobile */ } }\nheader h1 {\n  font-family: 'Arbutus', cursive;\n  text-align: center;\n  font-size: 61px;\n  color: #F8CA4D;\n  text-shadow: 0px 0px 4px #222; }\n@media (max-width: 767px) {\n  header h1 {\n    font-size: 41px; } }\nheader p {\n  text-align: center;\n  font-size: 21px; }\nheader .btn-link {\n  border: 1px solid #2F3440;\n  margin-top: 20px;\n  font-size: 21px;\n  letter-spacing: 1px;\n  color: #2F3440;\n  text-shadow: 0px 0px 1px #222;\n  -webkit-box-shadow: 0px 0px 1px #000;\n          box-shadow: 0px 0px 1px #000; }\nheader .btn-link:hover {\n  text-decoration: none;\n  color: #eee;\n  border: 1px solid #eee;\n  -webkit-transition: all 0.7s;\n  transition: all 0.7s;\n  text-shadow: 0px 0px 3px #000; }\n/* Custom navbar settings */\n.navbar-default {\n  background-color: #3F5666;\n  border-color: #2F3440;\n  border-bottom: 1px solidÂ #00c9c0;\n  border-radius: 0px; }\n.navbar-default .navbar-nav > li > a {\n  color: #fff;\n  letter-spacing: 1px;\n  font-weight: 500; }\n.navbar-default .navbar-brand {\n  color: #ffffff;\n  letter-spacing: 1px; }\n.navbar-default .navbar-brand:hover {\n  color: #eee;\n  -webkit-transition: all 0.7s;\n  transition: all 0.7s; }\n.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus {\n  color: #eee;\n  background-color: #2F3440;\n  -webkit-transition: all 0.7s;\n  transition: all 0.7s;\n  font-size: 21px; }\n.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus {\n  color: #eee;\n  background-color: #2F3440;\n  -webkit-transition: all 0.7s;\n  transition: all 0.7s;\n  font-size: 15px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layout/patient-home/patient-home/patient-home.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var wow_min_1 = __webpack_require__("../../../../wowjs/dist/wow.min.js");
var PatientHomeComponent = (function () {
    function PatientHomeComponent() {
    }
    PatientHomeComponent.prototype.ngOnInit = function () {
    };
    PatientHomeComponent.prototype.ngAfterViewInit = function () {
        new wow_min_1.WOW().init();
    };
    PatientHomeComponent = __decorate([
        core_1.Component({
            selector: 'app-patient-home',
            template: __webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PatientHomeComponent);
    return PatientHomeComponent;
}());
exports.PatientHomeComponent = PatientHomeComponent;


/***/ }),

/***/ "../../../../../src/app/layout/patient-home/patient-home/patient-home.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var patient_home_routing_module_1 = __webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home-routing.module.ts");
var patient_home_component_1 = __webpack_require__("../../../../../src/app/layout/patient-home/patient-home/patient-home.component.ts");
var PatientHomeModule = (function () {
    function PatientHomeModule() {
    }
    PatientHomeModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                patient_home_routing_module_1.PatientHomeRoutingModule,
            ],
            declarations: [
                patient_home_component_1.PatientHomeComponent,
            ]
        })
    ], PatientHomeModule);
    return PatientHomeModule;
}());
exports.PatientHomeModule = PatientHomeModule;


/***/ })

});
//# sourceMappingURL=layout.module.chunk.js.map