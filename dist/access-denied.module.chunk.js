webpackJsonp(["access-denied.module"],{

/***/ "../../../../../src/app/access-denied/access-denied-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var access_denied_component_1 = __webpack_require__("../../../../../src/app/access-denied/access-denied.component.ts");
var routes = [
    {
        path: '', component: access_denied_component_1.AccessDeniedComponent
    }
];
var AccessDeniedRoutingModule = (function () {
    function AccessDeniedRoutingModule() {
    }
    AccessDeniedRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], AccessDeniedRoutingModule);
    return AccessDeniedRoutingModule;
}());
exports.AccessDeniedRoutingModule = AccessDeniedRoutingModule;


/***/ }),

/***/ "../../../../../src/app/access-denied/access-denied.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  access-denied works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/access-denied/access-denied.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/access-denied/access-denied.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AccessDeniedComponent = (function () {
    function AccessDeniedComponent() {
    }
    AccessDeniedComponent.prototype.ngOnInit = function () {
    };
    AccessDeniedComponent = __decorate([
        core_1.Component({
            selector: 'app-access-denied',
            template: __webpack_require__("../../../../../src/app/access-denied/access-denied.component.html"),
            styles: [__webpack_require__("../../../../../src/app/access-denied/access-denied.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AccessDeniedComponent);
    return AccessDeniedComponent;
}());
exports.AccessDeniedComponent = AccessDeniedComponent;


/***/ }),

/***/ "../../../../../src/app/access-denied/access-denied.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var access_denied_routing_module_1 = __webpack_require__("../../../../../src/app/access-denied/access-denied-routing.module.ts");
var access_denied_component_1 = __webpack_require__("../../../../../src/app/access-denied/access-denied.component.ts");
var AccessDeniedModule = (function () {
    function AccessDeniedModule() {
    }
    AccessDeniedModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                access_denied_routing_module_1.AccessDeniedRoutingModule
            ],
            declarations: [access_denied_component_1.AccessDeniedComponent]
        })
    ], AccessDeniedModule);
    return AccessDeniedModule;
}());
exports.AccessDeniedModule = AccessDeniedModule;


/***/ })

});
//# sourceMappingURL=access-denied.module.chunk.js.map