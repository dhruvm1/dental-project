webpackJsonp(["appointment-page.module"],{

/***/ "../../../../../src/app/layout/appointment/appointment-page-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var appointment_page_component_1 = __webpack_require__("../../../../../src/app/layout/appointment/appointment-page.component.ts");
var routes = [
    {
        path: '',
        component: appointment_page_component_1.AppointmentPageComponent
    }
];
var AppointmentPageRoutingModule = (function () {
    function AppointmentPageRoutingModule() {
    }
    AppointmentPageRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], AppointmentPageRoutingModule);
    return AppointmentPageRoutingModule;
}());
exports.AppointmentPageRoutingModule = AppointmentPageRoutingModule;


/***/ }),

/***/ "../../../../../src/app/layout/appointment/appointment-page.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"w3ls-banner animated fadeIn\">\n    <div class=\"container\">\n        <div class=\"row\">\n            <div class=\"landing-page\">\n                <div class=\"form-appointment\">\n                    <div class=\"container\" id=\"wpcf7-f560-p590-o1\">\n                        <form class=\"wpcf7-form\" _lpchecked=\"1\" [formGroup]=\"appointmentForm\">\n                            <div class=\"row group\">\n                                <div class=\"col-md-6 col-sm-4\" style=\"float: left;\">\n                                    <span class=\"wpcf7-form-control-wrap text-680\">\n                                        <input type=\"text\" name=\"text-680\" value=\"\" size=\"45\" class=\"wpcf7-form-control wpcf7-text wpcf7-validates-as-required\" aria-required=\"true\"\n                                            placeholder=\"Name\" value=\"{{name}}\">\n                                    </span>\n                                    <br>\n                                    <span class=\"wpcf7-form-control-wrap email-680\">\n                                        <input type=\"email\" name=\"email-680\" value=\"\" size=\"45\" class=\"wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email\"\n                                            aria-required=\"true\" placeholder=\"Email\" value=\"{{email}}\">\n                                    </span>\n                                    <br>\n                                    <span class=\"wpcf7-form-control-wrap tel-630\">\n                                        <input type=\"tel\" name=\"tel-630\" value=\"\" size=\"45\" class=\"wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel\"\n                                            aria-required=\"true\" placeholder=\"Phone\" value=\"{{phone}}\">\n                                    </span>\n                                    <br>\n                                    <span class=\"wpcf7-form-control-wrap textarea-398\">\n                                        <textarea name=\"textarea-398\" cols=\"40\" rows=\"10\" class=\"wpcf7-form-control wpcf7-textarea\" placeholder=\"Special notes, concerns, or requirements\"></textarea>\n                                    </span>\n                                </div>\n                                <div class=\"col-md-6 col-sm-4\" style=\"float: right;\">\n                                    <h4>What is the best way to reach you?</h4>\n                                    <p>\n                                        <span class=\"wpcf7-form-control-wrap radio-98\">\n                                            <span class=\"wpcf7-form-control wpcf7-radio\">\n                                                <mat-radio-group formControlName=\"radval\">\n                                                    <mat-radio-button value=\"phone\">Phone</mat-radio-button>\n                                                    <mat-radio-button value=\"email\">Email</mat-radio-button>\n                                                </mat-radio-group>\n                                            </span>\n                                        </span>\n                                    </p>\n                                    <h4>Days of the week you are available for appointment:</h4>\n                                    <table style=\" margin: 0 auto;\">\n                                        <tr *ngFor=\"let day of days\">\n                                            <th>\n                                                <mat-checkbox (change)=\"onChange(day, $event.checked)\"></mat-checkbox>\n                                            </th>\n                                            <th>{{day}}</th>\n                                        </tr>\n                                    </table>\n                                    <h4>Best time of day for your appointment:</h4>\n                                    <p>\n                                        <span *ngFor=\"let time of time\">\n                                            <mat-checkbox (change)=\"onTime(time, $event.checked)\">{{time}}</mat-checkbox>\n                                        </span>\n                                    </p>\n                                </div>\n                            </div>\n                            <div style=\"text-align: center; padding-top: 2em; margin-top: 1em;\">\n                                <input type=\"submit\" class=\"wpcf7-form-control wpcf7-submit\" (click)=\"onRequest()\" value=\"Request My Appointment\" />\n                            </div>\n                        </form>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/layout/appointment/appointment-page.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".group:after {\n  content: \"\";\n  display: block;\n  clear: both; }\n\n.landing-page {\n  background: rgba(0, 0, 0, 0.4);\n  background-size: cover;\n  min-height: 100vh;\n  width: 100%;\n  color: #fff;\n  background-position: center; }\n\n.w3ls-banner {\n  background: url(\"https://cdn-images-1.medium.com/max/1300/1*RdHmw65n-8wpjQmPdEgnjA.jpeg\") no-repeat;\n  background-size: cover;\n  min-height: 100vh;\n  width: 100%;\n  color: #fff;\n  background-position: center;\n  text-align: center; }\n\n.form-appointment {\n  padding-top: 5em;\n  padding-left: 0.5em;\n  -webkit-box-shadow: 2px 2px 4px 0px rgba(0, 0, 0, 0.1);\n          box-shadow: 2px 2px 4px 0px rgba(0, 0, 0, 0.1);\n  font-family: 'PT Sans', Arial, sans-serif;\n  margin: 20px auto; }\n\n.form-appointment h4 {\n  font-weight: bold; }\n\n.form-appointment input[type=text],\n.form-appointment input[type=email],\n.form-appointment input[type=tel],\n.form-appointment textarea {\n  border: 1px solid #99ca81;\n  border-radius: .2em;\n  font-family: 'PT Sans', Arial, sans-serif;\n  font-size: 1.1em;\n  padding: .5em 1em;\n  margin: 0 0 1em;\n  width: 100%;\n  -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, 0.08) inset;\n          box-shadow: 0 0 8px rgba(0, 0, 0, 0.08) inset; }\n\n.form-appointment input[type=text],\n.form-appointment input[type=email],\n.form-appointment input[type=tel],\n.form-appointment input[type=submit],\n.form-appointment textarea:focus {\n  border-color: #ff80ff;\n  -webkit-box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset, 0px 0px 8px rgba(255, 100, 255, 0.5);\n          box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset, 0px 0px 8px rgba(255, 100, 255, 0.5);\n  -webkit-transition: all .2s ease-in-out;\n  transition: all .2s ease-in-out; }\n\n.form-appointment input[type=text]:active,\n.form-appointment input[type=text]:focus,\n.form-appointment input[type=email]:active,\n.form-appointment input[type=email]:focus,\n.form-appointment input[type=tel]:active,\n.form-appointment input[type=tel]:focus,\n.form-appointment textarea:active,\n.form-appointment textarea:focus {\n  outline: 0;\n  -webkit-box-shadow: 0 0 6px #b0e2bc;\n          box-shadow: 0 0 6px #b0e2bc; }\n\n.form-appointment textarea {\n  height: 160px; }\n\n.form-appointment input[type=submit] {\n  background-color: #76cf76;\n  border: 1px solid #86bd86;\n  border-radius: 4px;\n  color: white;\n  cursor: pointer;\n  font-family: inherit;\n  font-size: 1.4em;\n  padding: 10px 18px; }\n\n.form-appointment input[type=submit]:hover {\n  background-color: white;\n  color: #76cf76; }\n\n.form-appointment .wpcf7-list-item-label {\n  color: #82b288; }\n\nspan.wpcf7-list-item {\n  display: block;\n  margin-left: -.02em; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/layout/appointment/appointment-page.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var decode = __webpack_require__("../../../../jwt-decode/lib/index.js");
var AppointmentPageComponent = (function () {
    function AppointmentPageComponent(fb) {
        this.fb = fb;
        this.isChecked = false;
        this.days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
        this.time = ['Morning', 'Afternoon'];
    }
    AppointmentPageComponent.prototype.ngOnInit = function () {
        this.appointmentForm = this.fb.group({
            radval: new forms_1.FormControl(''),
            day: this.fb.array([]),
            time: this.fb.array([])
        });
        var token = localStorage.getItem('infoToken');
        var tokenPayload = decode(token);
        this.name = tokenPayload.firstname + ' ' + tokenPayload.lastname;
        this.email = tokenPayload.email;
        this.phone = tokenPayload.mobile_number;
    };
    AppointmentPageComponent.prototype.onChange = function (day, isChecked) {
        var days = this.appointmentForm.controls.day;
        if (isChecked) {
            days.push(new forms_1.FormControl(day));
        }
        else {
            var index = days.controls.findIndex(function (x) { return x.value == day; });
            days.removeAt(index);
        }
    };
    AppointmentPageComponent.prototype.onTime = function (time, isChecked) {
        var times = this.appointmentForm.controls.time;
        if (isChecked) {
            times.push(new forms_1.FormControl(time));
        }
        else {
            var index = times.controls.findIndex(function (x) { return x.value == time; });
            times.removeAt(index);
        }
    };
    AppointmentPageComponent.prototype.onRequest = function () {
    };
    AppointmentPageComponent = __decorate([
        core_1.Component({
            selector: 'app-blank-page',
            template: __webpack_require__("../../../../../src/app/layout/appointment/appointment-page.component.html"),
            styles: [__webpack_require__("../../../../../src/app/layout/appointment/appointment-page.component.scss")]
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder])
    ], AppointmentPageComponent);
    return AppointmentPageComponent;
}());
exports.AppointmentPageComponent = AppointmentPageComponent;


/***/ }),

/***/ "../../../../../src/app/layout/appointment/appointment-page.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var appointment_page_routing_module_1 = __webpack_require__("../../../../../src/app/layout/appointment/appointment-page-routing.module.ts");
var appointment_page_component_1 = __webpack_require__("../../../../../src/app/layout/appointment/appointment-page.component.ts");
var checkbox_1 = __webpack_require__("../../../material/esm5/checkbox.es5.js");
var radio_1 = __webpack_require__("../../../material/esm5/radio.es5.js");
var material_1 = __webpack_require__("../../../material/esm5/material.es5.js");
var AppointmentPageModule = (function () {
    function AppointmentPageModule() {
    }
    AppointmentPageModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, appointment_page_routing_module_1.AppointmentPageRoutingModule, checkbox_1.MatCheckboxModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, radio_1.MatRadioModule, material_1.MatFormFieldModule],
            declarations: [appointment_page_component_1.AppointmentPageComponent]
        })
    ], AppointmentPageModule);
    return AppointmentPageModule;
}());
exports.AppointmentPageModule = AppointmentPageModule;


/***/ })

});
//# sourceMappingURL=appointment-page.module.chunk.js.map