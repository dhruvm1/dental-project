webpackJsonp(["server-error.module"],{

/***/ "../../../../../src/app/server-error/server-error-routing.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var server_error_component_1 = __webpack_require__("../../../../../src/app/server-error/server-error.component.ts");
var routes = [
    {
        path: '', component: server_error_component_1.ServerErrorComponent
    }
];
var ServerErrorRoutingModule = (function () {
    function ServerErrorRoutingModule() {
    }
    ServerErrorRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], ServerErrorRoutingModule);
    return ServerErrorRoutingModule;
}());
exports.ServerErrorRoutingModule = ServerErrorRoutingModule;


/***/ }),

/***/ "../../../../../src/app/server-error/server-error.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\n  server-error works!\n</p>\n"

/***/ }),

/***/ "../../../../../src/app/server-error/server-error.component.scss":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/server-error/server-error.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var ServerErrorComponent = (function () {
    function ServerErrorComponent() {
    }
    ServerErrorComponent.prototype.ngOnInit = function () {
    };
    ServerErrorComponent = __decorate([
        core_1.Component({
            selector: 'app-server-error',
            template: __webpack_require__("../../../../../src/app/server-error/server-error.component.html"),
            styles: [__webpack_require__("../../../../../src/app/server-error/server-error.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ServerErrorComponent);
    return ServerErrorComponent;
}());
exports.ServerErrorComponent = ServerErrorComponent;


/***/ }),

/***/ "../../../../../src/app/server-error/server-error.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var common_1 = __webpack_require__("../../../common/esm5/common.js");
var server_error_routing_module_1 = __webpack_require__("../../../../../src/app/server-error/server-error-routing.module.ts");
var server_error_component_1 = __webpack_require__("../../../../../src/app/server-error/server-error.component.ts");
var ServerErrorModule = (function () {
    function ServerErrorModule() {
    }
    ServerErrorModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                server_error_routing_module_1.ServerErrorRoutingModule
            ],
            declarations: [server_error_component_1.ServerErrorComponent]
        })
    ], ServerErrorModule);
    return ServerErrorModule;
}());
exports.ServerErrorModule = ServerErrorModule;


/***/ })

});
//# sourceMappingURL=server-error.module.chunk.js.map