const express = require("express");
const router = express.Router();
var passport = require('passport');
var jwt = require('jsonwebtoken');
var config = require('../db/database');
var information = require('../model/patient-info-model');
var InfoSchema = require('../db/patient-info-crud');

router.post('/patient-register', function (req, res) {
    const operation = require("../db/crud");
    operation.searchForRegistration(req.body, res);
});

router.post('/patient-login', function (req, res) {
    const operation = require("../db/crud");
    operation.searchForLogin(req.body, res);
});

router.post('/storePatientInformation', function (req, res) {
    var token = req.headers['authorization'];
    if (!token) {
        return res.status(401).send({ auth: false, message: 'No token provided.' });
    }
    else {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                return res.status(401).send({ success: false, msg: 'Unauthorized.' });
            }
            else {
                var email = decoded['email'];
                var info = req.body;
                var infoOfPatient = new information(email, info);
                InfoSchema.add(infoOfPatient, res);
            }
        });
    }
});

module.exports = router;